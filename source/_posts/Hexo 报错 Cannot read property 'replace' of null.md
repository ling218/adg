---
title: Hexo 报错 Cannot read property 'replace' of null
comments: true
tags: hexo
categories: blog
keywords: replace
abbrlink: 6ff3b9a5
description:
---

Cannot read property 'replace' of null 在网上查询了一会资料，都说是站点配置`_config.yml`文件中的 URL 设置错误。

<!--more-->



我的设置：
```
# URL
## If your site is put in a subdirectory, set url as 'http://yoursite.com/child' and root as '/child/'
url: https://www.ling218.cn
root: /
permalink: archives/:abbrlink.html
abbrlink:
  alg: crc32
  rep: hex
```
域名可以正常访问网站，那会不会是root设置问题？于是改成：
```
# URL
## If your site is put in a subdirectory, set url as 'http://yoursite.com/child' and root as '/child/'
url: https://www.ling218.cn
root: ling218.github.io
permalink: archives/:abbrlink.html
abbrlink:
  alg: crc32
  rep: hex
```

再次执行 `hexo g -d` 命令，没有报错信息。