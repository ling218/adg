---
title: 如何在 Kali Linux 2019.2 安装 Google Chrome 浏览器
comments: true
tags: kali
categories: linux
keywords: 'kali,google'
description: kali linux  安装 Google Chrome 浏览器
abbrlink: 43353b59
---

Google Chrome 浏览器是世界上最爱欢迎的网络浏览器，快速、直观和安全的浏览器。

Chrome 不是一个开源浏览器，它不包含在 Kali Linux 存储库中。

Google Chrome 浏览器基于 Chromium，这是一个开源浏览器，可在 Debian 存储库中找到。

在本文中，我们将介绍如何在  Kali Linux 2019.2 上安装 Google Chrome 浏览器。



## 要求：

- 安装 Kali Linux 2019.2 系统的主机
- Google Chrome 软件
- 时间和耐心



## 安装：

完成以下步骤实现在 Kali Linux 系统上安装 Google Chrome 浏览器：

### 下载 Google Chrome 浏览器

使用 `Ctrl+Alt+T` 键盘快捷键或单击终端图标打开终端。

运行以下 `wget` 命令下载最新 Google Chrome 浏览器 `.deb` 包。

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
```

### 安装 Google Chrome 浏览器

下载完成后，键入以下内容安装 Google Chrome 浏览器。

```
apt-get update
apt-get install gdebi -y
gdebi google-chrome-stable_current_amd64.deb
```

### 启动 Google Chrome 浏览器

现在您已经在 Kali Linux 系统上安装了 Google Chrome，可以通过键入 `google-chrome --no-sandbox` 启动它。

### 更新 Google Chrome 浏览器

在安装过程中，官方 google 存储库将添加到系统中，可以使用 `cat` 命令验证文件内容：

```
cat /etc/apt/sources.list.d/google-chrome.list
```

```
deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main
```

发布新版本后，可以通过在终端中运行以下命令来更新 Google Chrome 软件。

```
apt-get update
apt-get upgrade
```







