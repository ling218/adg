---
title: 专业创建自定义实时 Debian Live CD
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,linux,debian'
description: 创建 LiveCD
abbrlink: 3be04a71
---



## 安装创建环境软件

```
# apt install live-build
```



## 加快 DebianLiveCD 创建速度

```
# mkdir -p /etc/live
# vim /etc/live/build.conf
LB_MIRROR_BOOTSTRAP="http://mirrors.163.com/debian/"
LB_MIRROR_CHROOT_SECURITY="http://mirrors.163.com/debian/"
LB_MIRROR_CHROOT_BACKPORTS="http://mirrors.163.com/debian/"
```



## 创建 DebianLiveCD

创建构建的过程如下：

1. 我们`lb config` 在空白目录中执行，生成一堆文件，这些文件代表了我们将来的ISO的文件结构。我们将参数传递给 *lb config*来个性化这些文件的生成。
2. 我们通过手动修改文件结构来进一步个性化这些文件结构：添加要在ISO生成的不同步骤或ISO引导时执行的脚本，将文件添加到ISO的用户文件夹等。
3. 我们执行`lb build`，读取这些文件和脚本并神奇地构建ISO，从而生成预期的 *.iso*文件。



### 准备工作

```
# mkdir live
# cd live
# cp /usr/share/doc/live-build/examples/auto/* auto/
# cat auto/config
#!/bin/sh

set -e

lb config noauto \
        "${@}"
```



### 添加个性化配置

```
# vim auto/config
#!/bin/sh

set -e

lb config noauto \
    --mode debian \
    --architectures i386 \
    --debian-installer false \
    --archive-areas "main contrib non-free" \
    --apt-indices false \
    --memtest none \
    "${@}"
```



### 为LiveCD安装软件

```
# echo vlc >> config/package-lists/my.list.chroot
```



### 开始创建

```
# lb clean
# lb build
```



## 原文地址

- https://www.bustawin.com/create-a-custom-live-debian-9-the-pro-way/#Speed-up_the_building_optional
- https://github.com/ereuse/workbench-live

