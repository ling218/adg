---
title: Debian 执行 sudo 不用输入密码
comments: true
tags: sudo
categories: linux
keywords: 'linux,sudo'
description: Debian 执行 sudo 不用输入密码
abbrlink: c80a599a
---


在 `/etc/sudoers` 文件尾添加下面一行：

```
username ALL=(ALL)NOPASSWD:ALL 
```

