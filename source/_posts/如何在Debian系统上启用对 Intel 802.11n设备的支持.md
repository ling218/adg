---
title: 如何在 Debian 系统上启用对 Intel 802.11n 设备的支持
comments: true
tags: Debian
categories: linux
keywords: 802.11n
description: 在 Debian 系统启用无线设备
abbrlink: 22c37512
---



## 该iwlwifi Linux内核驱动程序支持多种英特尔无线LAN适配器：
- 英特尔无线WiFi 5100AGN，5300AGN和5350AGN
- 英特尔无线WiFi 5150AGN
- 英特尔WiFi Link 1000BGN
- 英特尔6000系列WiFi适配器（6200AGN和6300AGN）
- 英特尔无线WiFi Link 6250AGN适配器
- 英特尔6005系列WiFi适配器
- 英特尔6030系列WiFi适配器
- 英特尔无线WiFi Link 6150BGN 2适配器
- 英特尔100系列WiFi适配器（100BGN和130BGN）
- 英特尔2000系列WiFi适配器
- 英特尔7260 WiFi适配器
- 英特尔7265 WiFi适配器
- 英特尔3160 WiFi适配器
- 英特尔3165 WiFi适配器

## 安装无线驱动

### Debian 9 "Stretch"

1. 在`/etc/apt/sources.list`中添加“非自由”组件，例如：
```
#Debian 9“Stretch”
deb http://httpredir.debian.org/debian/ stretch main contrib non-free
```
2. 更新可用软件包列表并安装firmware-iwlwifi软件包：
```
＃apt-get update && apt-get install firmware-iwlwifi
```
3. 当iwlwifi模块自动加载支持的设备时，重新插入此模块以访问已安装的固件：
```
#modprobe -r iwlwifi; modprobe iwlwifi
```
4. 根据需要配置无线接口。

### Debian 8“Jessie”
1. 在`/etc/apt/sources.list`中添加“非自由”组件，例如：
```
#Debian 8“Jessie”
deb http://httpredir.debian.org/debian/ jessie main contrib non-free
```
2. 更新可用软件包列表并安装firmware-iwlwifi软件包：
```
＃apt-get update && apt-get install firmware-iwlwifi
```
3. 当iwlwifi模块自动加载支持的设备时，重新插入此模块以访问已安装的固件：
```
#modprobe -r iwlwifi; modprobe iwlwifi
```
4. 根据需要配置无线接口。

### Debian 7“Wheezy”

1. 在`/etc/apt/sources.list`中添加“非自由”组件，例如：
```
#Debian 7“Wheezy”
deb http://httpredir.debian.org/debian/ wheezy main contrib non-free
```
2. 更新可用软件包列表并安装firmware-iwlwifi软件包：
```
＃apt-get update && apt-get install firmware-iwlwifi
```
3. 当iwlwifi模块自动加载支持的设备时，重新插入此模块以访问已安装的固件：
```
#modprobe -r iwlwifi; modprobe iwlwifi
```
4. 根据需要配置无线接口。

## 故障排除

### 使用蓝牙时WiFi速度慢

像6235这样的设备在同一张卡中包含蓝牙设备，这可能导致无线电冲突。较新的设备（7200及以上）尝试智能地解决它们，但不是旧设备的情况。

如果使用蓝牙时WiFi速度很慢，请尝试将以下内容添加到`/etc/modprobe.d/iwlwifi.conf`并重新启动：
```
options iwlwifi bt_coex_active = 0 swcrypto = 1 11n_disable = 8
```
