---
title: VMware Workstation 设置 efi 启动方法
comments: true
tags: EFI
categories: other
keywords: 'VMware Workstation,EFI'
description: 设置虚拟机启动方式BIOS或EFI
abbrlink: 26de0019
---



看到很多人写了修改虚拟机目录下vmx文件的方法，但是这个方法改完了efi启动后我不知道怎么才能改回bios启动。

其实选中虚拟机-编辑虚拟机设置-选项-高级，右边的设置，有个选项叫通过efi而非bios引导。这个勾了就efi，不勾就是bios，还蛮方便的。