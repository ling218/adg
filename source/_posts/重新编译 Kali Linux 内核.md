---
title: 重新编译 Kali Linux 内核
comments: true
tags: kernl
categories:
  - linux
keywords: 'kali,linux,kernl'
description: 您可能希望添加库存Kali Linux内核中未包含的驱动程序，补丁或内核功能
abbrlink: a9289db8
---

Kali Linux的可定制性一直扩展到Linux内核。

根据您的要求，您可能希望添加库存Kali Linux内核中未包含的驱动程序，补丁或内核功能。以下指南将介绍如何快速修改和重新编译Kali Linux内核以满足您的需求。请注意，默认情况下，Kali Linux内核中已经存在全局无线注入补丁。

## 安装构建依赖项

首先安装所有用于重新编译内核的构建依赖项。

```
apt install build-essential libncurses5-dev fakeroot unxz
```

## 下载 Kali Linux 内核源代码

本节的其余部分重点介绍4.9版本的Linux内核，但是示例当然可以根据所需的特定内核版本进行调整。我们假设已安装linux-source-4.9二进制软件包。请注意，我们安装了包含上游源代码的二进制软件包，但没有检索名为linux的Kali源代码软件包。

```
apt install linux-source-4.9
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  bc libreadline7
Suggested packages:
  libncurses-dev | ncurses-dev libqt4-dev
The following NEW packages will be installed:
  bc libreadline7 linux-source-4.9
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 95.4 MB of archives.
After this operation, 95.8 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
...SNIP...
ls /usr/src
linux-config-4.9  linux-patch-4.9-rt.patch.xz  linux-source-4.9.tar.xz
```

请注意，该软件包包含*/usr/src/linux-source-4.9.tar.xz*，这是内核源代码的压缩存档。您必须将这些文件解压缩到新目录中（而不是直接在/ usr / src /下，因为不需要特殊权限来编译Linux内核）。相反，*〜/ kernel /*更合适。

```
mkdir ~/kernel; cd ~/kernel
tar -xaf /usr/src/linux-source-4.9.tar.xz
```

## 配置内核

重新编译内核的最新版本（可能带有附加补丁程序）时，配置很可能会尽可能与Kali提出的配置保持接近。在这种情况下，将*/ boot / config-version*文件（版本是当前使用的内核的版本，可以通过**uname -r**命令找到）复制到*本地*，而不是从头开始重新配置所有内容*。*包含内核源代码的目录中的*配置*文件。

```
cp /boot/config-4.9.0-kali1-amd64 ~/kernel/linux-source-4.9/.config
```

如果需要进行更改，或者决定从头开始重新配置所有内容，则必须花时间配置内核。这可以通过调用**make menuconfig**命令来完成。

```
make menuconfig
```

使用**menuconfig**设置内核构建的详细信息不在本指南的范围之内。[有关](https://www.linux.org/threads/the-linux-kernel-configuring-the-kernel-part-1.8745/)在Linux.org [上配置内核构建](https://www.linux.org/threads/the-linux-kernel-configuring-the-kernel-part-1.8745/)的[详细教程](https://www.linux.org/threads/the-linux-kernel-configuring-the-kernel-part-1.8745/)。

## 建立内核

准备好内核配置后，一个简单的**make deb-pkg**将生成多达5个Debian软件包：包含内核映像和相关模块的*linux-image- **版本*** *linux-headers- **version***，其中包含构建所需的头文件。外部模块*linux-firmware-image- **version***，其中包含一些驱动程序所需的固件文件（当从Debian或Kali提供的内核源进行构建时，可能会缺少此软件包），*linux-image- **version** -dbg*，其中包含内核映像及其模块和*linux-libc-dev*的调试符号，其中包含与某些用户空间库（例如GNU glibc）相关的标头。Linux内核映像是一个大型构建，需要花费一些时间才能完成。

```
make clean
make deb-pkg LOCALVERSION=-custom KDEB_PKGVERSION=$(make kernelversion)-1
...SNIP...
ls ../*.deb
../linux-headers-4.9.0-kali1-custom_4.9.2-1_amd64.deb
../linux-image-4.9.0-kali1-custom_4.9.2-1_amd64.deb
../linux-image-4.9.0-kali1-custom-dbg_4.9.2-1_amd64.deb
../linux-libc-dev_4.9.2-1_amd64.deb
```

## 安装修改后的内核

构建成功完成后，您可以继续安装新的自定义内核并重新引导系统。请注意，特定的内核版本号会有所不同-在我们的示例中，是在Kali 2016.2系统上完成的，为4.9.2。根据要构建的当前内核版本，您将需要相应地调整命令。

```
dpkg -i ../linux-image-4.9.0-kali1-custom_4.9.2-1_amd64.deb
reboot
```

系统重新引导后，新内核应已运行。如果出现问题并且内核无法成功引导，您仍然可以使用GrUB菜单从原始的原始Kali内核引导并解决问题。