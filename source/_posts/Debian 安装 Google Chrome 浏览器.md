---
title: Debian 安装 Google Chrome 浏览器
comments: true
tags: chrome
categories: linux
keywords: 'linux,chrome'
description: Debian 安装 Google Chrome 浏览器
abbrlink: c945e1f0
---


1、首先需要下载并安装谷歌官方秘钥：

```
# wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
```

2、然后是添加谷歌官方软件源：

```
# echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
```

3、执行安装命令

```
# apt-get update
# apt-get -y install google-chrome-stable
```

