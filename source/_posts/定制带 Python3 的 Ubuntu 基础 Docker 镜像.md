---
title: 定制带 Python3 的 Ubuntu 基础 Docker 镜像
comments: true
tags: python3
categories: python
keywords: 'python3,docker'
description: 定制Dcoker镜像
abbrlink: d5246505
---


## 制作Dockerfile
```
FROM daocloud.io/ubuntu:trusty
MAINTAINER water-law <dockerwaterlaw@daocloud.io>
RUN apt-get update && \
    apt-get install -y python3 \
                        python3-dev \
                        python3-pip \
    && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*
    
RUN mkdir -p /app
WORKDIR /app
EXPOSE 80
CMD ["bash"]
```

## 生成镜像
创建空文件夹， 将 Dockerfile 文件放在此目录。
```
运行 docker build -t [tagname] .
```

## 导出镜像

```
docker images
```
```
docker save -o [output_name] [image]
```

其中 output_name 一般是 tar, tar.xz 格式。

## 利用镜像生成容器

```
docker run -it -v E:Projectswaterlawblog:/home/code [image] /bin/bash
```

## 退出容器

```
exit
```

## 再次启动容器

```
docker start [OPTIONS] CONTAINER [CONTAINER...]
```

说明: 对容器的修改是永久保存到文件系统的。

可发现`E:Projectswaterlawblog`是挂载到 docker 容器的`/home/code`目录的。

## 修改容器

比如 安装 virtualenv 且在 /home 目录使用 virtualenv -p python3 pysp

创建虚拟环境， 进入 /home/code, 运行 pip3 install -r requirements.txt

## 提交容器修改

先退出容器 exit

基于旧的 container 提交新的 docker 镜像

```
docker commit -m "commit message" -a "author info" [container_id] image_name:tag
```

示例：

```
docker commit -m "earth" -a "zjp" 846edc550555 kuaiyun:earth
```
提示： 以上命令会打了一个 REPOSITORY 为 kuaiyun、 TAG 为 earth 的镜像， 可通过`docker images`查看。

## 导出容器

```
docker ps -a
```

```
| CONTAINER I D| IMAGE | COMMAND | CREATED STATUS | PORTS | NAMES |

| caa1f263a254 | myubuntu-base | "/bin/bash" | 32 minutes ago Exited (0) 54 seconds ago | | relaxed_lumiere |

docker export -o [outputfile] [container_id]
```

## 导入容器 tar 文件

```
docker import [container_file] [command]
```

我这里 command 是 /bin/bash, 所以 docker import [container_file] bash。

## 导入镜像 tar 文件

```
docker load < [tar file]
```

注意 save 和 load 是配对的， import 和 export 是配对的。 import 还要加上 command。