---
title: 实时构建自定义的 Kali ISO
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,kali,linux'
description: 实时构建自定义的Kali ISO，被用来制作Kali ISO正式发行版
abbrlink: fb169ff8
---

## 建立自己的Kali ISO简介

构建自定义的Kali ISO既简单，有趣又有意义。您几乎可以使用Debian动态[构建](https://live-team.pages.debian.net/live-manual/html/live-manual/index.en.html)脚本来配置Kali ISO构建的任何方面。这些脚本使开发人员可以通过提供一个框架来轻松构建实时系统映像，该框架使用配置集来自动化和自定义构建映像的所有方面。Kali Linux开发团队已经采用了这些脚本，它们被用来制作Kali ISO正式发行版。

## 您应该在哪里建立ISO？

理想情况下，您应该**在现有的Kali环境中**构建自定义的Kali ISO 。

## 准备就绪-设置实时构建系统

我们首先需要使用以下命令通过安装和设置实时构建及其要求来准备Kali ISO构建环境：

```
apt install -y curl git live-build cdebootstrap
git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git
```

现在，您只需输入“ live-build-config”目录并运行我们的**build.sh**包装器脚本，即可简单地构建更新的Kali ISO ，如下所示：

```
cd live-build-config/
./build.sh --verbose
```

“ build.sh”脚本需要一段时间才能完成，因为它会下载创建ISO所需的所有必需软件包。喝咖啡的好时机。

## 配置Kali ISO构建（可选）

如果您想自定义您的Kali Linux ISO，本节将解释一些细节。通过**kali-config**目录，Kali Linux live build支持各种自定义选项，这些选项在Debian [live build 4.x](https://live-team.pages.debian.net/live-manual/html/live-manual/customization-overview.en.html)页面上有详细记录。但是，对于不耐烦的人，这里是一些重点。

### 使用不同的桌面环境构建Kali

从Kali 2.0开始，我们现在支持各种桌面环境的内置配置，包括KDE，Gnome，E17，I3WM，LXDE，MATE和XFCE。要构建其中的任何一个，您将使用类似于以下内容的语法：

```
# These are the different Desktop Environment build options:
#./build.sh --variant {gnome,kde,xfce,mate,e17,lxde,i3wm} --verbose

# To build a KDE ISO:
./build.sh --variant kde --verbose
# To build a MATE ISO:
./build.sh --variant mate --verbose

#...and so on.
```

### 控制构建中包含的软件包

您的构建中包含的软件包列表将出现在相应的kali- $ variant目录中。例如，如果要构建默认的Gnome ISO，则应使用以下软件包列表文件**-kali-config / variant-gnome / package-lists / kali.list.chroot**。默认情况下，此列表包括“ kali-linux-full”元软件包以及其他一些软件包。可以将它们注释掉，并用手册的手动列表替换，以将其包括在ISO中以获得更大的粒度。

### 构建钩子，二进制文件和chroot

实时构建挂钩允许我们在Kali ISO实时构建的各个阶段中钩挂脚本。有关钩子及其使用方法的更多详细信息，请参阅[实时构建手册](https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-contents.en.html#507)。例如，我们建议您在**kali-config / common / hooks /中**检出现有的挂钩。

### 覆盖构建中的文件

您可以通过将其他文件或脚本分别覆盖在现有文件系统上的**include。{chroot，binary，installer}**目录中，来选择将其他文件或脚本包含在构建中。例如，如果我们想将自己的自定义脚本包括在ISO 的**/ root /**目录中（这将对应于“ chroot”阶段），则可以将该脚本文件放入**kali-config / common / includes中。 chroot /**目录，然后再建立ISO。

## 为较早的i386架构构建Kali Linux ISO

Kali Linux i386 ISO已启用PAE。如果您需要禁用PAE的旧硬件的默认内核，则需要重建Kali Linux ISO。重建过程与上述过程基本相同，除了需要按如下所示在**auto / config**中将**686-pae**参数更改为**586**。首先，安装先决条件。

```
apt install -y git live-build cdebootstrap debootstrap
git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git
```

接下来，对auto / config进行更改以适合相应的体系结构：

```
cd live-build-config/
sed -i 's/686-pae/686/g' auto/config
```

最后，运行您的构建。

```
./build.sh --arch i386 --verbose
```

