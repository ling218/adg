---
title: 使用 dd 命令制作 USB 启动盘
comments: true
tags: dd
categories: linux
keywords: 'linux,dd'
description: 使用U盘安装系统需要把U盘做成启动盘
abbrlink: 87ebfc6b
---



一般情况下，我会使用U盘安装Linux和其他操作系统。

使用U盘安装系统需要把U盘做成启动盘。

启动U盘的制作工具有很多：
- Unetbootin
- Win32diskimager
- Linux live usb
- USB image writer
- WinUSB
- USB startup creator

如果你使用的是Linux，最简单快捷的方法是使用dd命令。步骤是：首先下载要安装操作系统的镜像，然后使用dd命令创建可启动U盘。

dd命令的使用看这里：Linux dd命令使用示例

制作启动U盘步骤

1）格式化U盘。为了格式化我们首先需要 umount U盘：

查看设备：
```
sudo fdisk -l
```

/dev/sdb是我的U盘设备，umount：
```
sudo umount /dev/sdb*
```

格式化U盘：
```
sudo mkfs.vfat /dev/sdb –I
```

上面命令把U盘格式化为FAT格式。

2）制作启动U盘。
```
sudo dd if=~/home/bibi/Ubuntu_15_10_64.iso of=/dev/sdb
```

上面命令把ISO镜像写入到U盘，等待几分钟。

补：mac os x也有dd命令，下面介绍一下怎么在mac os x系统上制作启动U盘。

查看存储设备的命令：
```
diskutil list
```

使用dd命令拷贝ISO镜像：
```
sudo dd if=kali-linux-light-2016.1-amd64.iso of=/dev/disk3
```