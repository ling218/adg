---
title: Next7.x 添加背景图片
comments: true
tags: next7.6
categories: blog
keywords: 'hexo,next7.6'
description: 添加next主题博客背景图片
abbrlink: 81b35757
---



打开**主题配置文件** `themes/next/_config.yml` ，搜索找到 custom_file_path ，将下列代码取消注释。
```
style: source/_data/styles.styl
```

## 站点根目录
```
mkdir -p source/_data
```
```
vim source/_data/styles.styl

// 添加背景图片
body {
      background: url(https://source.unsplash.com/random/1600x900?wallpapers);//自己喜欢的图片地址
      background-size: cover;
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: 50% 50%;
}

// 修改主体透明度
.main-inner {
      background: #fff;
      opacity: 0.8;
}

// 修改菜单栏透明度
.header-inner {
      opacity: 0.8;
}
```

