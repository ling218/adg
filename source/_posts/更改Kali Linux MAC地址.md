---
title: 更改 Kali Linux MAC 地址
comments: true
tags: MAC
categories: linux
keywords: 'linux,MAC'
description: 防止某些软件记录你真实的MAC地址
abbrlink: c566ca23
---


## 更改计算机的MAC地址,使用情形如下:

- 防止某些软件记录你真实的MAC地址
- 网络管理员屏蔽了你的MAC地址

## 查看原始MAC地址:

```
ifconfig
```
我以无线网卡(wlan0)为例,00:17:c4:bb:13:b7。有线网卡是eth0,方法一样。

## 为了临时改变mac地址,我们首先关闭这个网卡:

```
ifconfig wlan0 down
```
配置新的mac地址:

```
ifconfig wlan0 hw ether 00:00:00:00:00:11
```
mac地址格式是十六进制:XX:XX:XX:XX:XX:XX

## 开启网卡接口:

```
ifconfig wlan0 up
```
上面的方法是临时生效,在系统重启之后会恢复为初始的mac地址。如果你想永久生效,请看下一节。

## MAC地址永久生效:

Kail Linux基于Debian,而Debian的所有网络接口配置都位于/etc/network/interfaces文件中。

编辑/etc/network/interfaces:
```
vim /etc/network/interfaces
```
添加一行脚本:
```
pre-up ifconfig wlan0 hw ether 00:00:00:00:00:11
```
如果你想恢复初始mac地址,只要注释掉这一行就可以了。

## 使用macchanger更改mac地址

例如我要更改无线网卡的mac地址,依次执行如下命令:
```
ifconfig wlan0 down
macchanger -r wlan0
service network-manager restart
```
上面命令设置的mac地址是随机的。