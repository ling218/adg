---
title: 在 Debian10 系统上从软件包安装 Docker-CE
comments: true
tags: docker-ce
categories: linux
keywords: 'linux,docker-ce'
description: 软件包安装docker-ce
abbrlink: f9fb805e
---



这种方法安装Docker-CE，每次升级Docker时，都需要下载一个新的软件包 重复安装过程。

如果不是在新主机上首次安装 Docker-CE，为确保安装不会出错，可以执行卸载旧版本操作。

## 卸载旧版本

Docker 的旧版本被称为`docker`，`docker.io`或`docker-engine`。如果已安装，请卸载它们：

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```

## 安装步骤

1. 转到 https://download.docker.com/linux/debian/dists/，选择您Debina10的版本，下载要安装Docker-CE的版本文件。

2. 安装Docker-CE，将下面的路径更改为下载Docker软件包的存储路径。

   ```
   sudo dpkg -i /path/to/package.deb
   ```

3. 验证是否正解安装Docker-CE。

   ```
   sudo docker run hello-world
   ```

   此命令下载测试图像并在容器中运行它。容器运行时，它会打印参考消息并退出。

   

