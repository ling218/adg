---
title: 让 Google 搜索引擎重新抓取我的网站
comments: true
tags: google
categories: blog
keywords: "Google"
description: 让Google搜索引擎抓取我们的网站
abbrlink: 7a1bc012
---



## 提交站点地图 （很多个网址）

站点地图是 Google 发现您网站上网址的重要方法。

如果自 Google 上次抓取以来您并未更改站点地图，那么重新提交该站点地图不会获得任何其他好处。如果站点地图中有更新过的网页，请将其标记为 `<lastmod>`。

#### 以下方法可以提交站点地图：
- 使用站点地图报告提交站点地图。
- 使用 ping 工具。在浏览器或命令行中向此地址发送 GET 请求，并指定站点地图的完整网址。请确保 Sitemap 文件可供访问：
  http://www.google.com/ping?sitemap=<full_URL_of_sitemap>
示例：
  http://www.google.com/ping?sitemap=https://www.ling218.cn/sitemap.xml
- 将下面这行内容插入到 robots.txt 文件中的任意位置，以指定指向您站点地图的路径。我们会在下次抓取您的网站时找到该站点地图：
    Sitemap: http://example.com/my_sitemap.xml