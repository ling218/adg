---
title: 下载 Youtube 视频和播放列表
comments: true
tags: youtube-dl
categories: other
keywords: youtube-dl
abbrlink: 17677cc8
description:
---

youtube-dl是由某大佬开发的专攻YouTube视频下载的python脚本，使用简单、功能齐全、体积小巧。

<!--more-->

### 普通下载

```
youtube-dl 'https://www.youtube.com/watch?v=y8CPXkP2CKc&pbjreload=10'
```

### 下载列表
```
youtube-dl -cit 'https://www.youtube.com/watch?v=y8CPXkP2CKc&pbjreload=10'
```

### 使用代理下载
```
youtube-dl --proxy 127.0.0.1:1080 https://www.youtube.com/watch?v=y8CPXkP2CKc&pbjreload=10
```

### 输出文件的名字
```
youtube-dl 'https://www.youtube.com/watch?v=y8CPXkP2CKc&pbjreload=10' -o '%(title)s.%(ext)s'
```