---
title: 解决 Debian 终端命令行无法自动补全
comments: true
tags: debain
categories: linux
keywords: 'linux,debian'
description: '安装了debian,在终端输入一半命令行再按 <TAB>后，却没有实现自动补全功能。几经搜索，在一篇老外的无政府主义里得到了解决方案'
abbrlink: 7d602a45
---


1、安装命令补全：

```
# apt-get install bash-completion
```

2、在 `/etc/profile` 添加

```
if [ -f /etc/bash_completion ]; then
. /etc/bash_completion
fi
```

3、刷新 `/etc/profile` 配置文件，使其生效

```
# source /etc/profile
```

