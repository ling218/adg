---
title: Next7.6主题美化篇
comments: true
tags: Next7.6
categories: blog
keywords: 'hexo,next7.6'
abbrlink: cd68432f
description: 实操记录
---



## Next 主题的安装

Next主题安装和闪烁之狐一样，直接将克隆到博客文件夹即可，命令如下

```
git clone https://github.com/theme-next/hexo-theme-next themes/next
```
由于Next主题比较简洁，所以等待数秒即可安装完成。
由于Next主题一直在更新，所以此处加一个更新命令：

```
cd themes/next
git pull
```

安装完成后到根目录下的`_config.yml`在`theme：`后添加`next`即可。

## Next 主题的配置

安装完成后还需要进一步配置主题

## 根目录下`_config.yml`的配置

### Site设置

```css
# Site
title: LinG218
subtitle: LinG218-Blog!
description: 技术宅男！
keywords:
author: ling
language: zh-CN
```

`title:` 填写标题
`subtitle:` 填写副标题
`description:` 简介
`keywords:` 关键词
`author:` 作者，文章中显示的作者名称
`language:` 设置语言，zh-CN是简体中文，en是英语，默认是英语

### URL栏设置

```css
# URL
## If your site is put in a subdirectory, set url as 'http://yoursite.com/child' and root as '/child/'
url: https://Siriusq.github.io
root: /
permalink: :title.html
permalink_defaults:
```

`url:`填写博客在Github的二级域名，如 `https://ling218.github.io`

`permalink:`是生成博客页面时的路径名，一般是`:title.html`

### 翻页设置

共有两处，用于控制每一页显示多少篇博文

```css
# Home page setting
# path: Root path for your blogs index page. (default = '')
# per_page: Posts displayed per page. (0 = disable pagination)
# order_by: Posts order. (Order by date descending by default)
index_generator:
  path: ''
  per_page: 12
  order_by: -date

# Pagination
## Set per_page to 0 to disable pagination
per_page: 12
pagination_dir: page
```

`per_page:`后设置博文数量，0代表不翻页全部平铺显示

### Deployment设置

```css
# Deployment
## Docs: https://hexo.io/docs/deployment.html
deploy:
  type: git
  repo: git@github.com:username/username.github.io.git
  branch: master
```

`branch:`分支填写`master`

`repo:`后把`username`换成自己的用户名

### 主题目录下`_config.yml`的配置

**文件中很多配置前面都带有`#`，删掉它就可以激活选项**

### 网站Logo设置

```css
favicon:
  small: /images/favicon-16x16-next.png
  medium: /images/favicon-32x32-next.png
  apple_touch_icon: /images/apple-touch-icon-next.png
  #safari_pinned_tab: /images/logo.svg
  #android_manifest: /images/manifest.json
  #ms_browserconfig: /images/browserconfig.xml
```

把选好的图标放入`\themes\next\source\images`目录中并改成上面的文件名即可，注意图片尺寸

### 页脚设置

```css
footer:
  # Specify the date when the site was setup. If not defined, current year will be used.
  since: 2019

  # Icon between year and copyright info.
  icon:
    # Icon name in fontawesome, see: https://fontawesome.com/v4.7.0/icons/
    # `heart` is recommended with animation in red (#ff0000).
    name: heart
    # If you want to animate the icon, set it to true.
    animated: true
    # Change the color of icon, using Hex Code.
    color: "#f08080"

  # If not defined, `author` from Hexo main config will be used.
  copyright: Siriusq

  powered:
    # Hexo link (Powered by Hexo).
    enable: false
    # Version info of Hexo after Hexo link (vX.X.X).
    version: false

  theme:
    # Theme & scheme info link (Theme - NexT.scheme).
    enable: false
    # Version info of NexT after scheme info (vX.X.X).
    version: false
```

`since:` 表示网站成立的年份，不设置的话默认是当前年份，显示为`© 2019`

`icon:` 中设置年份后面的图标，默认是心形，想修改的话去[Fontawesome](https://fontawesome.com/)选择图标，然后再`name`栏修改

`animated:` 用于设置心形是否跳动`color:` 用于设置图标颜色，百度对照色表查就好

`copyright:` 显示图标后面的版权所属，也就是作者名，不设置的话默认显示根目录配置文件中的作者名

`powered:` 后面的栏目用于设定是否显示`Powered by Hexo`以及当前主题的版本信息，显示的话设为`true`

`theme:` 后面的栏目用于设定是否显示当前主题的版本信息，显示的话将设为`true`



### 版权信息

```css
creative_commons:
  license: by-nc-sa
  sidebar: true
  post: true
  language: en
```

`license:`用于设置版权标准，具体可以去[Creative Commons](https://creativecommons.org/share-your-work/licensing-types-examples)查看
`sidebar:`用于设置是否在侧边栏显示版权标准
`post:`用于设置是否在文章底部显示版权信息
`language:`用于设置语言



### Github角标

```css
# `Follow me on GitHub` banner in the top-right corner.
github_banner:
  enable: true
  permalink: https://github.com/Siriusq
  title: Follow me on GitHub
```

`enable:`用于设置是否在页面右上角显示Github三角标
`permalink:`后面填写你的Github地址
`title:`用于设置鼠标移动到图标后显示的文字



### 侧边栏菜单设置

```css
menu:
  home: / || home
  about: /about/ || user
  tags: /tags/ || tags
  categories: /categories/ || th
  archives: /archives/ || archive
  switch_lang: /en || language
  #schedule: /schedule/ || calendar
  #sitemap: /sitemap.xml || sitemap
  #commonweal: /404/ || heartbeat
```

`||`前表示页面的地址，后面表示显示的图标名称
`about:`用于设置`关于`页面，没有的话在Git bash中使用`hexo new page "about"`创建，然后打开`\source\about\index.md`就可以编写了
`tags:`用于设置`标签`功能，同样使用命令`hexo new page "tags"`创建，然后打开`source\tags\index.md`并在日期后面添加一行`type: "tags"`，写博文时在顶部加上一栏`tags: []`即可设置标签，多个标签中间使用`,`分隔
`archieves:`用于设置归档页面
`categories:`用于设置目录页面，编辑博文时在顶部加上一栏`categories:`就可以为博文设置所属目录



### 主题选择

Next有四个主题可供选择

```css
# Schemes
#scheme: Muse
#scheme: Mist
#scheme: Pisces
scheme: Gemini
```

把想要的主题前面的`#`删掉即可



### 社交网站链接设置

在侧边栏可以设置显示社交链接，让更多人了解自己

```css
# Social Links
# Usage: `Key: permalink || icon`
# Key is the link label showing to end users.
# Value before `||` delimeter is the target permalink.
# Value after `||` delimeter is the name of FontAwesome icon. If icon (with or without delimeter) is not specified, globe icon will be loaded.
social:
  GitHub: https://github.com/Siriusq || github
  E-Mail: mailto:siriuskevin@foxmail.com || envelope
  #Weibo: https://weibo.com/yourname || weibo
  #Google: https://plus.google.com/yourname || google
  #Twitter: https://twitter.com/yourname || twitter
  #FB Page: https://www.facebook.com/yourname || facebook
  #VK Group: https://vk.com/yourname || vk
  #StackOverflow: https://stackoverflow.com/yourname || stack-overflow
  #YouTube: https://youtube.com/yourname || youtube
  #Instagram: https://instagram.com/yourname || instagram
  #Skype: skype:yourname?call|chat || skype
```

在`||`前输入你社交主页的链接即可，后面用于设置图标
`E-mail:`栏格式为`mailto:邮箱地址`，在win10系统可以调用系统邮件服务自动填写收件人



### 头像设置

用于设置是否在侧边栏显示头像

```css
# Sidebar Avatar
avatar:
  # In theme directory (source/images): /images/avatar.gif
  # In site directory (source/uploads): /uploads/avatar.gif
  # You can also use other linking images.
  url: /images/avatar.gif
  # If true, the avatar would be dispalyed in circle.
  rounded: true
  # The value of opacity should be choose from 0 to 1 to set the opacity of the avatar.
  opacity: 1
  # If true, the avatar would be rotated with the cursor.
  rotated: true
```

在`url:`后填写头像图片的路径，默认在`\themes\next\source\images`中
`rounded:`用于设置是否显示圆形头像
`opacity:`用于设置头像透明度，1为不透明
`rotated:`用于设置头像鼠标移动到头像上时头像是否旋转



### 侧边栏目录设置

```css
# Table Of Contents in the Sidebar
toc:
  enable: true
  # Automatically add list number to toc.
  number: true
  # If true, all words will placed on next lines if header width longer then sidebar width.
  wrap: false
  # If true, all level of TOC in a post will be displayed, rather than the activated part of it.
  expand_all: false
  # Maximum heading depth of generated toc. You can set it in one post through `toc_max_depth` in Front Matter.
  max_depth: 6
```

`enable:`用于设置是否开启侧边栏目录
`number:`用于设置自动编号
`wrap:`用于设置当标题长度超过侧边栏长度时是否自动换行
`expand_all:`用于设置是否展开全部目录，否的话会自动展开正在看的那部分
`max_depth:`用于设置自动生成目录的最大深度，也就是生成到几级标题



### 侧边栏显示设置

```css
sidebar:
  # Sidebar Position, available values: left | right (only for Pisces | Gemini).
  #position: left
  position: right

  # Manual define the sidebar width. If commented, will be default for:
  # Muse | Mist: 320
  # Pisces | Gemini: 240
  width: 320

  # Sidebar Display, available values (only for Muse | Mist):
  #  - post    expand on posts automatically. Default.
  #  - always  expand for all pages automatically.
  #  - hide    expand only when click on the sidebar toggle icon.
  #  - remove  totally remove sidebar including sidebar toggle.
  display: post

  # Sidebar offset from top menubar in pixels (only for Pisces | Gemini).
  offset: 12
  # Enable sidebar on narrow view (only for Muse | Mist).
  onmobile: false
  # Click any blank part of the page to close sidebar (only for Muse | Mist).
  dimmer: false
```

`position:`用于设置侧边栏在左边还是右边，把要选择的位置前面的`#`去掉再把另一个前面加上`#`即可，这个选项只对`Pisces`和`Gemini`两个主题起作用
`width:`用于设置侧边栏的宽度，默认`Muse`和`Mist`是320,`Pisces`和`Gemini`是240
`display:`用于设置侧边栏的显示方式，只对`Muse`和`Mist`两个主题起作用，可以挨个试一下
`offset:`用于设置侧边栏距离顶部介绍的距离，只对`Pisces`和`Gemini`两个主题起作用
`onmobile:`用于设置是否在手机等宽度较小的设备上显示侧边栏，只对`Muse`和`Mist`两个主题起作用
`dimmer:`用于设置点击屏幕空白处是否关闭侧边栏，同样只对`Muse`和`Mist`两个主题起作用



### 返回顶部按钮

```css
back2top:
  enable: true
  # Back to top in sidebar.
  sidebar: true
  # Scroll percent label in b2t button.
  scrollpercent: true
```

`enable:`用于设置是否开启回到顶部按钮
`sidebar:`用于设置是否将按钮放到侧边栏中
`scrollpercent:`用于设置是否显示阅读进度百分比



### 开启`阅读全文`选项

用搜索找到这一段并把`read_more_btn:`设为`true`即可

```css
# Read more button
# If true, the read more button would be displayed in excerpt section.
read_more_btn: true
```

下面这一段用于设置自动生成`阅读全文`选项

```css
# Automatically Excerpt (Not recommend).
# Use <!-- more --> in the post to control excerpt accurately.
auto_excerpt:
  enable: false
  length: 150
```

`length`表示截止到的字符长度

这一段用于设置自动跳转到`阅读全文`选项之后的位置

```css
# Automatically scroll page to section which is under <!-- more --> mark.
scroll_to_more: true
```

设置完成后，在文章内加入`<!--more-->`字段就可以在首页隐去剩下的文字并显示`阅读全文` 按钮了



### 代码块复制按钮

```css
codeblock:
  # Manual define the border radius in codeblock, leave it blank for the default value: 1
  border_radius:
  # Add copy button on codeblock
  copy_button:
    enable: true
    # Show text copy result
    show_result: true
    # Style: only 'flat' is currently available, leave it blank if you prefer default theme
    style:
```

`enable：`用于开启代码块右上角的复制按钮
`show_result:`用于设置是否显示复制成功提示



### 打赏

```css
# Reward (Donate)
reward_settings:
  # If true, reward would be displayed in every article by default.
  # You can show or hide reward in a specific article throuth `reward: true | false` in Front Matter.
  enable: false
  animation: false
  #comment: Donate comment here

reward:
  #wechatpay: /images/wechatpay.png
  #alipay: /images/alipay.png
  #bitcoin: /images/bitcoin.png
```

`enable:`用于设置是否在文章末尾显示打赏按钮
`animation:`用于设置打上按钮是否显示动画
`reward:`后面的选项去掉前面的`#`即可开启对应的打赏，记得替换成自己的图片，路径在`\themes\next\source\images`



### 代码块高亮主题

Next主题内置了tomorrow代码高亮主题，一共有五个，可以去[Tomorrow](https://github.com/chriskempson/tomorrow-theme)查看预览效果，在下面的`highlight_theme:`中改成自己想要的即可

```css
# Code Highlight theme
# Available values: normal | night | night eighties | night blue | night bright
# https://github.com/chriskempson/tomorrow-theme
highlight_theme: night
```



### 书签

在左上角显示一个书签按钮可以跳转到上一次设置书签的位置
首先执行命令`git clone https://github.com/theme-next/theme-next-bookmark.git source/lib/bookmark`安装[bookmark](https://github.com/theme-next/theme-next-bookmark)插件
然后在下面的选项中将`enable:`设置为`true`

```css
# Bookmark Support
# Dependencies: https://github.com/theme-next/theme-next-bookmark
bookmark:
  enable: true
  # If auto, save the reading position when closing the page or clicking the bookmark-icon.
  # If manual, only save it by clicking the bookmark-icon.
  save: auto
```



### 开启RSS

话说RSS好像也没什么人用了。。。。。。

#### 安装

在博客根目录执行下面的命令安装RSS插件

```dos
npm install hexo-generator-feed --save
```

#### 配置

打开博客根目录的配置文件`_config.yml`并在末尾添加下列代码

```css
# RSS订阅支持
plugin:
- hexo-generator-feed

# Feed Atom
feed:
type: atom
path: atom.xml
limit: 20
```

### 页面搜索功能

开启站内搜索功能方便快速定位

#### 安装

执行下列命令安装插件

```dos
npm install hexo-generator-searchdb --save
```

#### 配置

打开Next主题的配置文件`_config.yml`并搜索`local_search`,修改为

```css
# Local search
# Dependencies: https://github.com/theme-next/hexo-generator-searchdb
local_search:
  enable: true
  # If auto, trigger search by changing input.
  # If manual, trigger search by pressing enter key or search button.
  trigger: auto
  # Show top n results per article, show all results by setting to -1
  top_n_per_article: 1
  # Unescape html strings to the readable one.
  unescape: false
```



## Next 主题深度美化

## 顶部阅读进度条

在博文页面顶部添加[Reading Progress](https://github.com/theme-next/theme-next-reading-progress)进度条，表示阅读进度，Next主题已内置配置文件

### 安装

切换到Next主题文件夹

```dos
cd themes/next
```

安装模块到`source/lib`文件夹

```dos
git clone https://github.com/theme-next/theme-next-reading-progress source/lib/reading_progress
```

### 修改配置文件

打开Next主题路径下的配置文件`_config.yml`，搜索`reading_progress`定位到如下代码并将`enable:`设置为`true`

```css
# Reading progress bar
# Dependencies: https://github.com/theme-next/theme-next-reading-progress
reading_progress:
  enable: true
  color: "#ffc0cb" # 调整线条颜色
  height: 2px # 调整线条高度
```

线条颜色可以在[ATOOL](http://www.atool9.com/colorpicker.php)查询颜色代码直接修改，默认颜色是青色，我修改的是粉色
顶部阅读进度条可能会和加载条重合，可以通过修改线条高度覆盖，我的线条高度设置成了`3px`

### 升级

切换到安装目录

```dos
cd themes/next/source/lib/reading_progress
```

更新

```dos
git pull
```



## 加载条

用于在网页加载的过程中显示加载进度，Next主题已支持[PACE](https://github.com/theme-next/theme-next-pace)功能
有多种加载动画可选

- pace-theme-big-counter
- pace-theme-bounce
- pace-theme-barber-shop
- pace-theme-center-atom
- pace-theme-center-circle
- pace-theme-center-radar
- pace-theme-center-simple
- pace-theme-corner-indicator
- pace-theme-fill-left
- pace-theme-flash
- pace-theme-loading-bar
- pace-theme-mac-osx
- pace-theme-minimal

### 安装

切换到Next主题文件夹

```dos
cd themes/next
```

安装模块到`source/lib`文件夹

```dos
git clone https://github.com/theme-next/theme-next-pace source/lib/pace
```

### 修改配置文件

打开Next主题路径下的配置文件`_config.yml`，搜索`pace`定位到如下代码

```js
# Progress bar in the top during page loading.
# Dependencies: https://github.com/theme-next/theme-next-pace
pace: true
# Themes list:
# pace-theme-big-counter | pace-theme-bounce | pace-theme-barber-shop | pace-theme-center-atom
# pace-theme-center-circle | pace-theme-center-radar | pace-theme-center-simple | pace-theme-corner-indicator
# pace-theme-fill-left | pace-theme-flash | pace-theme-loading-bar | pace-theme-mac-osx | pace-theme-minimal
pace_theme: pace-theme-flash
```

将`pace:`设置为`true`
将`pace_theme:`设置为上面`Themes list:`中的一种，主题样式可以在[PACE](https://github.hubspot.com/pace/docs/welcome/)查看

### 调整颜色

为了让进度条和主题背景相匹配，可以在`./themes/next/source/lib/pace`中找到相应主题的css文件并修改颜色配置
下面代码以`pace-theme-flash.min.css`示例

```css
.pace {
  -webkit-pointer-events: none;
  pointer-events: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #ffc0cb;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}

.pace .pace-progress-inner {
  display: block;
  position: absolute;
  right: 0px;
  width: 100px;
  height: 100%;
  box-shadow: 0 0 10px #ffc0cb, 0 0 5px #ffc0cb;
  opacity: 1.0;
  -webkit-transform: rotate(3deg) translate(0px, -4px);
  -moz-transform: rotate(3deg) translate(0px, -4px);
  -ms-transform: rotate(3deg) translate(0px, -4px);
  -o-transform: rotate(3deg) translate(0px, -4px);
  transform: rotate(3deg) translate(0px, -4px);
}

.pace .pace-activity {
  display: block;
  position: fixed;
  z-index: 2000;
  top: 15px;
  right: 15px;
  width: 14px;
  height: 14px;
  border: solid 2px transparent;
  border-top-color: #ffc0cb;
  border-left-color: #ffc0cb;
  border-radius: 10px;
  -webkit-animation: pace-spinner 400ms linear infinite;
  -moz-animation: pace-spinner 400ms linear infinite;
  -ms-animation: pace-spinner 400ms linear infinite;
  -o-animation: pace-spinner 400ms linear infinite;
  animation: pace-spinner 400ms linear infinite;
}

@-webkit-keyframes pace-spinner {
  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
}
@-moz-keyframes pace-spinner {
  0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
}
@-o-keyframes pace-spinner {
  0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
}
@-ms-keyframes pace-spinner {
  0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
}
@keyframes pace-spinner {
  0% { transform: rotate(0deg); transform: rotate(0deg); }
  100% { transform: rotate(360deg); transform: rotate(360deg); }
}
```

使用搜索功能搜索`#`,定位到颜色设置，上述代码中一共有5处，颜色替换方法如下

- 在[ATOOL](http://www.atool9.com/colorpicker.php)查询颜色代码直接修改
- 使用`Visual Studio Code`安装css插件直接点击`#`修改颜色
- 前往[PACE](https://github.hubspot.com/pace/docs/welcome/)选择颜色并复制代码直接替换原文件中的代码

默认颜色都是蓝色，上述代码修改完后是粉色

### 升级

切换到安装目录

```dos
cd themes/next/source/lib/pace
```

更新

```dos
git pull
```



## 动态背景

Next主题默认提供了三种动态背景

- [Canvas-nest](https://github.com/theme-next/theme-next-canvas-nest)
- [JavaScript 3D library](https://github.com/theme-next/theme-next-three)
- [Canvas-ribbon](https://github.com/theme-next/theme-next-canvas-ribbon)

### Canvas-nest

我选用的是这个背景，比较好玩

#### 安装

切换到Next主题文件夹

```dos
cd themes/next
```

安装模块

```dos
git clone https://github.com/theme-next/theme-next-canvas-nest source/lib/canvas-nest
```

#### 修改配置文件

打开Next主题路径下的配置文件`_config.yml`修改下列代码

```js
# Canvas-nest
# Dependencies: https://github.com/theme-next/theme-next-canvas-nest
canvas_nest:
  enable: true
  onmobile: true # 是否在手机上显示
  color: "255,51,51" # RGB颜色设置
  opacity: 0.5 # 线条透明度
  zIndex: -1 # 显示级别
  count: 160 # 线条的数量，越多越卡
```

如果想使用CDN加速的话需要设置

```js
vendors:
  ...
  canvas_nest: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@1.0.0/canvas-nest.min.js
  canvas_nest_nomobile: //cdn.jsdelivr.net/gh/theme-next/theme-next-canvas-nest@1.0.0/canvas-nest-nomobile.min.js
```

#### 升级

切换到安装目录

```dos
cd themes/next/source/lib/canvas-nest
```

更新

```dos
git pull
```

### JavaScript 3D library

#### 安装

切换到Next主题文件夹

```dos
cd themes/next
```

安装模块

```dos
git clone https://github.com/theme-next/theme-next-three source/lib/three
```

#### 修改配置文件

打开Next主题路径下的配置文件`_config.yml`修改下列代码，有三种风格可选，将一种设置为`true`即可

```js
# JavaScript 3D library.
# Dependencies: https://github.com/theme-next/theme-next-three
# three_waves
three_waves: true
# canvas_lines
canvas_lines: false
# canvas_sphere
canvas_sphere: false
```

#### 升级

切换到安装目录

```dos
cd themes/next/source/lib/three
```

更新

```dos
git pull
```

### Canvas-ribbon

#### 安装

切换到Next主题文件夹

```dos
cd themes/next
```

安装模块

```dos
git clone https://github.com/theme-next/theme-next-canvas-ribbon source/lib/canvas-ribbon
```

#### 修改配置文件

打开Next主题路径下的配置文件`_config.yml`修改下列代码

```js
# Canvas-ribbon
# Dependencies: https://github.com/theme-next/theme-next-canvas-ribbon
# size: The width of the ribbon.
# alpha: The transparency of the ribbon.
# zIndex: The display level of the ribbon.
canvas_ribbon:
  enable: true # 是否开启
  size: 300 # 宽度
  alpha: 0.6 # 透明度
  zIndex: -1 # 显示级别
```

#### 升级

切换到安装目录

```dos
cd themes/next/source/lib/canvas-ribbon
```

更新

```dos
git pull
```



## 鼠标点击特效

在网页中加入鼠标点击特效，目前有心形和爆炸两种特效，两者可以叠加，特效对性能较低的设备非常不友好，尤其是爆炸特效，在平板上卡成PPT，慎用……

### 心形特效

#### 创建js文件

在`./themes/next/source/js`中新建`clicklove.js`，并将粘贴下列代码

```js
!function(e,t,a){function n(){c(".heart{width: 10px;height: 10px;position: fixed;background: #f00;transform: rotate(45deg);-webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);}.heart:after,.heart:before{content: '';width: inherit;height: inherit;background: inherit;border-radius: 50%;-webkit-border-radius: 50%;-moz-border-radius: 50%;position: fixed;}.heart:after{top: -5px;}.heart:before{left: -5px;}"),o(),r()}function r(){for(var e=0;e<d.length;e++)d[e].alpha<=0?(t.body.removeChild(d[e].el),d.splice(e,1)):(d[e].y--,d[e].scale+=.004,d[e].alpha-=.013,d[e].el.style.cssText="left:"+d[e].x+"px;top:"+d[e].y+"px;opacity:"+d[e].alpha+";transform:scale("+d[e].scale+","+d[e].scale+") rotate(45deg);background:"+d[e].color+";z-index:99999");requestAnimationFrame(r)}function o(){var t="function"==typeof e.onclick&&e.onclick;e.onclick=function(e){t&&t(),i(e)}}function i(e){var a=t.createElement("div");a.className="heart",d.push({el:a,x:e.clientX-5,y:e.clientY-5,scale:1,alpha:1,color:s()}),t.body.appendChild(a)}function c(e){var a=t.createElement("style");a.type="text/css";try{a.appendChild(t.createTextNode(e))}catch(t){a.styleSheet.cssText=e}t.getElementsByTagName("head")[0].appendChild(a)}function s(){return"rgb("+~~(255*Math.random())+","+~~(255*Math.random())+","+~~(255*Math.random())+")"}var d=[];e.requestAnimationFrame=function(){return e.requestAnimationFrame||e.webkitRequestAnimationFrame||e.mozRequestAnimationFrame||e.oRequestAnimationFrame||e.msRequestAnimationFrame||function(e){setTimeout(e,1e3/60)}}(),n()}(window,document);
```

#### 修改layout文件

打开`./themes/next/layout/_layout.swig`并在末尾添加以下代码

```js
<!-- 页面点击心形 -->
<script type="text/javascript" src="/js/clicklove.js"></script>
```

### 爆炸特效

#### 创建js文件

在`./themes/next/source/js`中新建`firework.js`，并将粘贴下列代码

```js
"use strict";function updateCoords(e){pointerX=(e.clientX||e.touches[0].clientX)-canvasEl.getBoundingClientRect().left,pointerY=e.clientY||e.touches[0].clientY-canvasEl.getBoundingClientRect().top}function setParticuleDirection(e){var t=anime.random(0,360)*Math.PI/180,a=anime.random(50,180),n=[-1,1][anime.random(0,1)]*a;return{x:e.x+n*Math.cos(t),y:e.y+n*Math.sin(t)}}function createParticule(e,t){var a={};return a.x=e,a.y=t,a.color=colors[anime.random(0,colors.length-1)],a.radius=anime.random(16,32),a.endPos=setParticuleDirection(a),a.draw=function(){ctx.beginPath(),ctx.arc(a.x,a.y,a.radius,0,2*Math.PI,!0),ctx.fillStyle=a.color,ctx.fill()},a}function createCircle(e,t){var a={};return a.x=e,a.y=t,a.color="#F00",a.radius=0.1,a.alpha=0.5,a.lineWidth=6,a.draw=function(){ctx.globalAlpha=a.alpha,ctx.beginPath(),ctx.arc(a.x,a.y,a.radius,0,2*Math.PI,!0),ctx.lineWidth=a.lineWidth,ctx.strokeStyle=a.color,ctx.stroke(),ctx.globalAlpha=1},a}function renderParticule(e){for(var t=0;t<e.animatables.length;t++){e.animatables[t].target.draw()}}function animateParticules(e,t){for(var a=createCircle(e,t),n=[],i=0;i<numberOfParticules;i++){n.push(createParticule(e,t))}anime.timeline().add({targets:n,x:function(e){return e.endPos.x},y:function(e){return e.endPos.y},radius:0.1,duration:anime.random(1200,1800),easing:"easeOutExpo",update:renderParticule}).add({targets:a,radius:anime.random(80,160),lineWidth:0,alpha:{value:0,easing:"linear",duration:anime.random(600,800)},duration:anime.random(1200,1800),easing:"easeOutExpo",update:renderParticule,offset:0})}function debounce(e,t){var a;return function(){var n=this,i=arguments;clearTimeout(a),a=setTimeout(function(){e.apply(n,i)},t)}}var canvasEl=document.querySelector(".fireworks");if(canvasEl){var ctx=canvasEl.getContext("2d"),numberOfParticules=30,pointerX=0,pointerY=0,tap="mousedown",colors=["#FF1461","#18FF92","#5A87FF","#FBF38C"],setCanvasSize=debounce(function(){canvasEl.width=2*window.innerWidth,canvasEl.height=2*window.innerHeight,canvasEl.style.width=window.innerWidth+"px",canvasEl.style.height=window.innerHeight+"px",canvasEl.getContext("2d").scale(2,2)},500),render=anime({duration:1/0,update:function(){ctx.clearRect(0,0,canvasEl.width,canvasEl.height)}});document.addEventListener(tap,function(e){"sidebar"!==e.target.id&&"toggle-sidebar"!==e.target.id&&"A"!==e.target.nodeName&&"IMG"!==e.target.nodeName&&(render.play(),updateCoords(e),animateParticules(pointerX,pointerY))},!1),setCanvasSize(),window.addEventListener("resize",setCanvasSize,!1)}"use strict";function updateCoords(e){pointerX=(e.clientX||e.touches[0].clientX)-canvasEl.getBoundingClientRect().left,pointerY=e.clientY||e.touches[0].clientY-canvasEl.getBoundingClientRect().top}function setParticuleDirection(e){var t=anime.random(0,360)*Math.PI/180,a=anime.random(50,180),n=[-1,1][anime.random(0,1)]*a;return{x:e.x+n*Math.cos(t),y:e.y+n*Math.sin(t)}}function createParticule(e,t){var a={};return a.x=e,a.y=t,a.color=colors[anime.random(0,colors.length-1)],a.radius=anime.random(16,32),a.endPos=setParticuleDirection(a),a.draw=function(){ctx.beginPath(),ctx.arc(a.x,a.y,a.radius,0,2*Math.PI,!0),ctx.fillStyle=a.color,ctx.fill()},a}function createCircle(e,t){var a={};return a.x=e,a.y=t,a.color="#F00",a.radius=0.1,a.alpha=0.5,a.lineWidth=6,a.draw=function(){ctx.globalAlpha=a.alpha,ctx.beginPath(),ctx.arc(a.x,a.y,a.radius,0,2*Math.PI,!0),ctx.lineWidth=a.lineWidth,ctx.strokeStyle=a.color,ctx.stroke(),ctx.globalAlpha=1},a}function renderParticule(e){for(var t=0;t<e.animatables.length;t++){e.animatables[t].target.draw()}}function animateParticules(e,t){for(var a=createCircle(e,t),n=[],i=0;i<numberOfParticules;i++){n.push(createParticule(e,t))}anime.timeline().add({targets:n,x:function(e){return e.endPos.x},y:function(e){return e.endPos.y},radius:0.1,duration:anime.random(1200,1800),easing:"easeOutExpo",update:renderParticule}).add({targets:a,radius:anime.random(80,160),lineWidth:0,alpha:{value:0,easing:"linear",duration:anime.random(600,800)},duration:anime.random(1200,1800),easing:"easeOutExpo",update:renderParticule,offset:0})}function debounce(e,t){var a;return function(){var n=this,i=arguments;clearTimeout(a),a=setTimeout(function(){e.apply(n,i)},t)}}var canvasEl=document.querySelector(".fireworks");if(canvasEl){var ctx=canvasEl.getContext("2d"),numberOfParticules=30,pointerX=0,pointerY=0,tap="mousedown",colors=["#FF1461","#18FF92","#5A87FF","#FBF38C"],setCanvasSize=debounce(function(){canvasEl.width=2*window.innerWidth,canvasEl.height=2*window.innerHeight,canvasEl.style.width=window.innerWidth+"px",canvasEl.style.height=window.innerHeight+"px",canvasEl.getContext("2d").scale(2,2)},500),render=anime({duration:1/0,update:function(){ctx.clearRect(0,0,canvasEl.width,canvasEl.height)}});document.addEventListener(tap,function(e){"sidebar"!==e.target.id&&"toggle-sidebar"!==e.target.id&&"A"!==e.target.nodeName&&"IMG"!==e.target.nodeName&&(render.play(),updateCoords(e),animateParticules(pointerX,pointerY))},!1),setCanvasSize(),window.addEventListener("resize",setCanvasSize,!1)};
```

#### 修改layout文件

打开`./themes/next/layout/_layout.swig`并在``中间添加以下代码

```js
  <!-- 爆炸效果 -->
<canvas class="fireworks" style="position: fixed;left: 0;top: 0;z-index: 1; pointer-events: none;" ></canvas> 
<script type="text/javascript" src="//cdn.bootcss.com/animejs/2.2.0/anime.min.js"></script> 
<script type="text/javascript" src="/js/firework.js"></script>
```

#### 配置主题文件

打开`./themes/next`路径下的`_config.yml`文件并在末尾添加以下代码

```js
# Fireworks
fireworks: true
```



## 网易云音乐挂件

给博客加入背景音乐，支持网易云歌单，步骤如下：

1. 首先登陆[网易云音乐网页版](https://music.163.com/)

2. 选择自己想要添加的歌单

3. 分享歌单

4. 点击右上角个人头像，进入`我的主页`

5. 点击`动态`

6. 找到自己刚刚分享的歌单

7. 点击`生成外链播放器`

8. 选择`iframe`界面并调整播放器样式与大小

9. `播放模式`个人建议不勾选自动播放

10. 复制下方html代码

11. 按照路径`Hexo根目录\themes\next\layout\_macro`打开`sidebar.swig`

12. 在合适的位置加入以下代码，位置慢慢调整,中间

    ```
    iframe
    ```

    是刚刚复制的代码

    ```js
     <div id="music163player">
            <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=280 height=450 src="//music.163.com/outchain/player?type=0&id=908252389&auto=1&height=430">
            </iframe>
            </div> 
    ```

13. 播放器可能由于侧栏宽度不够显示不全，需要在`主题配置文件`中调整侧栏宽度

14. 打开next主题下的`_config.yml`文件

15. 搜索width,在下列代码中去掉最后一行width前的

    ```
    #
    ```

    号使其生效

    ```
    # Manual define the sidebar width. If commented, will be default for:
    # Muse | Mist: 320
    # Pisces | Gemini: 240
    # width: 300
    ```

16. 根据需要调整侧栏宽度



## 修改超链接样式

打开`themes\next\source\css\_common\components\post\post.styl`并在末尾一串`@import`上面添加以下代码

```css
// 文章内链接文本样式
.post-body p a {
  color: #0593d3;
  border-bottom: none;
  border-bottom: 1px solid #0593d3;
  &:hover {
    color: #fc6423;
    border-bottom: none;
    border-bottom: 1px solid #fc6423;
  }
}
```



## 在文章结尾添加本文结束标志

### 新建文件

在`\themes\next\layout\_macro`中新建`passage-end-tag.swig`文件,并复制下列代码：

```js
<div>
    {% if not is_index %}
        <div style="text-align:center;color: #ccc;font-size:14px;">-------------本文结束<i class="fa fa-paw"></i>感谢您的阅读-------------</div>
    {% endif %}
</div>
```

### 修改文件

打开`\themes\next\layout\_macro\post.swig`，在`post-body`之后，`post-footer`之前添加下列代码

```js
<div>
  {% if not is_index %}
    {% include 'passage-end-tag.swig' %}
  {% endif %}
</div>
```

### 修改主题文件

打开主题配置文件`_config.yml`,并在末尾添加：

```css
# 文章末尾添加“本文结束”标记
passage_end_tag:
  enabled: true
```



## 网站运行时间

打开`.\themes\next\layout\_partials\footer.swig`并添加下列代码

```css
<div>
<span id="timeDate">载入天数...</span><span id="times">载入时分秒...</span>
<script>
    var now = new Date();
    function createtime() {
        var grt= new Date("03/31/2019 00:00:00");
        now.setTime(now.getTime()+250);
        days = (now - grt ) / 1000 / 60 / 60 / 24; dnum = Math.floor(days);
        hours = (now - grt ) / 1000 / 60 / 60 - (24 * dnum); hnum = Math.floor(hours);
        if(String(hnum).length ==1 ){hnum = "0" + hnum;} minutes = (now - grt ) / 1000 /60 - (24 * 60 * dnum) - (60 * hnum);
        mnum = Math.floor(minutes); if(String(mnum).length ==1 ){mnum = "0" + mnum;}
        seconds = (now - grt ) / 1000 - (24 * 60 * 60 * dnum) - (60 * 60 * hnum) - (60 * mnum);
        snum = Math.round(seconds); if(String(snum).length ==1 ){snum = "0" + snum;}
        document.getElementById("timeDate").innerHTML = "本站已安全运行 "+dnum+" 天 ";
        document.getElementById("times").innerHTML = hnum + " 小时 " + mnum + " 分 " + snum + " 秒";
    }
setInterval("createtime()",250);
</script>
</div>
```



## 访客统计

Next主题已经内置了不蒜子统计，在Next主题的配置文件`_config.yml`中搜索`busuanzi_count`并将`enable`设置为`true`，`icon`可以自定义设置，下面是我的配置

```css
# Show Views / Visitors of the website / page with busuanzi.
# Get more information on http://ibruce.info/2015/04/04/busuanzi
busuanzi_count:
  enable: true
  total_visitors: true # 总访客数量，在页脚显示
  total_visitors_icon: user
  total_views: true # 总阅读数量，在页脚显示
  total_views_icon: eye
  post_views: true # 单篇文章阅读数量，在标题下显示
  post_views_icon: eye
```



## 字数统计和阅读时长统计

### 安装

在博客根目录下执行命令安装统计插件

```dos
npm install hexo-symbols-count-time --save
```

### 配置

在博客根目录配置文件`_config.yaml`中加入下列代码

```css
symbols_count_time:
  symbols: true # 文章字数
  time: true # 阅读时长
  total_symbols: true # 所有文章总字数
  total_time: true # 所有文章阅读中时长
```

在Next主题配置文件`_config.yaml`中搜索`symbol_count`并修改为

```css
symbols_count_time:
  separated_meta: true
  item_text_post: true
  item_text_total: false
  awl: 4
  wpm: 275
```



## 图片懒加载

只在需要的时候加载图片，而不是一次性加载完成，加快速度，适用于图片较多的博客（~~像我一样懒得放图的就不用看了~~）

### 安装

在博客根目录执行命令安装插件

```dos
npm install hexo-lazyload --save
```

### 配置

在博客根目录的配置文件`_config.yml`中添加下列代码

```css
lazyload:
  enable: true
  onlypost: false #是否只在文章中开启懒加载
  # className: #可选 e.g. .J-lazyload-img
  # loadingImg: #可选 eg. ./images/loading.png 作为图片未加载时的替代图片
```



