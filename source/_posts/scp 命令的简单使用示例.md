---
title: scp 命令的简单使用示例
comments: true
tags: scp
categories: other
keywords: scp
description: 经常要用到的scp使用方法示例
abbrlink: 489403ef
---


## 上传本地文件(夹)到服务器

文件：

```
scp  /local/file/path/test.txt root@ip:/remote/path/
```

文件夹（加上`-r`参数）：

```
scp  -r /local/file/path/ root@ip:/remote/path/
```

使用秘钥上传文件夹时：

```
scp -i "1.pem" -r /local/file/path/ root@ip:/remote/path/
```

## 下载服务器文件(夹)到本地

文件：

```
scp root@ip:/remote/path/ /lcal/file/path
```

文件夹（加上`-r`参数）：

```
    scp -r root@ip:/remote/path/ /lcal/file/path/
```

使用秘钥下载文件夹时：

```
scp -i "1.pem" -r root@ip:/remote/path/ /lcal/file/path/
```

