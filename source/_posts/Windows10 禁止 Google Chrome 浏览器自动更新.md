---
title: Windows10 禁止 Google Chrome 浏览器自动更新
comments: true
tags: chrome
categories: windows
keywords: 'windows,chrome'
description: 禁止Google Chrome自动更新
abbrlink: 7f5a70e7
---


## 方法一：

快捷键 **Win + R** ，打开运行对话框。输入 `services.msc`， 进到服务管理窗口。

找到 Google 的两个更新的服务：

- Google更新服务(gupdate)
- Google更新服务(gupdatem)

分别选择两个更新服务，将其启动类型设置为“禁用”。


## 方法二：

快捷键 **Win+R**，打开运行对话框。输入 `taskschd.msc`，打开"任务计划程序"。

展开左侧功能树到"任务计划程序库"，

分两次选择右侧两个 GoogleUpdate 的任务计划，右键菜单选择"禁用"。

