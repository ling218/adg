---
title: Windows PE 与 Kali Linux 双系统U盘
comments: true
tags:
  - windows
  - kali
categories: other
keywords: 'windows,kali'
description: 双系统U盘
abbrlink: 2bf2b15c
---


## 准备的工具

- 一个大于8GB的U盘

- Windows PE和Kali Linux的两个iso镜像

  - 最好重命名镜像，在制作完成后启动项中将会显示镜像名

- YUMI刻入工具

  - 官网地址：<https://www.pendrivelinux.com/yumi-multiboot-usb-creator/>
  - 注意：`需下载UEFI版`

  ![PE-LINUX-YUMI-UEFI](https://nico.cc/images/screenshots/PE-LINUX-YUMI-UEFI.png)

  - 若无法访问，可使用本地下载
  - 本地链接: <https://nico.cc/softs/YUMI-UEFI-0.0.0.6.exe>

## 开始刻入

**首先将Windows PE刻录进U盘**

- 将U盘格式化为 `Fat32`或`NTFS` ,启动YUMI-UEFI软件
- 说明:`Fat32`兼容性最好，最大缺点就是单个文件不能超过4GB，如果U盘需要存放超过4GB文件，将U盘格式化为`NTFS`；若格式化为`exFAT`，会导致无法正常启动系统。
- 选择需要刻录的U盘

![PE-LINUX-Step-1](https://nico.cc/images/screenshots/PE-LINUX-Step-1.png)

- 在Step 2 的下拉栏中选择Windows 10 Installer

![PE-LINUX-Step-2](https://nico.cc/images/screenshots/PE-LINUX-Step-2.png)

- 在Step 3 中导入待刻录的Windows PE镜像

![PE-LINUX-Step-3](https://nico.cc/images/screenshots/PE-LINUX-Step-3.png)

- 选择完成后，点击 `Create` ，开始创建。

![PE-LINUX-Step-Create](https://nico.cc/images/screenshots/PE-LINUX-Step-Create.png)

- 然后慢慢等吧
- 刻录成功的话大概是这个样子

![PE-LINUX-Step-next](https://nico.cc/images/screenshots/PE-LINUX-Step-Next.png)

- 再点击 `Next` 会出现是否继续添加镜像刻录

![PE-LINUX-Step-add](https://nico.cc/images/screenshots/PE-LINUX-Step-add.png)

- 点击是，添加Linux系统

**再将Kali Linux刻录进U盘**

- 又回到第一步，选择U盘
- 在Step 2 的下拉栏中选择 Debian Live.
- `注意：如果你的Linux系统是CentOS或者Ubuntu，请选择对应的 Distribution`

![PE-LINUX-Step-22](https://nico.cc/images/screenshots/PE-LINUX-Step-22.png)

- 在Step 3 中导入待刻录的Linux镜像
- Step 4 中 询问 `“设置用于存储更改的固定文件大小(可选)”`
  - 如果你的U盘比较大，可以给4GB(能给的最大也就4GB)
  - 我的只有8GB，所以就不给了
- 选择完成后，点击 `Create` ，开始创建。
- 再继续慢慢等吧
- 刻入成功大概又是这个样子

![PE-LINUX-Step-33](https://nico.cc/images/screenshots/PE-LINUX-Step-33.png)

## 删除某个系统

- YUMI 这个软件也提供了删除的选项
- 再选择好U盘后， 右边会出现两个选项，选择 `“View or Remove Installed Distros”`--查看或删除已安装的版本

![PE-LINUX-Step-remove](https://nico.cc/images/screenshots/PE-LINUX-Step-remove.png)

- 在 Step 2 的下拉框中会出现U盘中所有已安装的系统，选择需要删除的即可删除

![PE-LINUX-Step-rv-li](https://nico.cc/images/screenshots/PE-LINUX-Step-rv-li.png)

## Kali Linux系统更换为root用户

- 默认的Kali Linux默认用户并不是root用户，并不具备所有权限，需要更换为root用户，默认root用户密码为`toor`。
- 

