---
title: dpkg 命令的用法
comments: true
tags: dpkg
categories: linux
keywords: 'linux,dpkg'
abbrlink: a1571737
description:
---

dpkg 是 Debian Package 的简写，为Debian操作系统专门开发的套件管理系统，用于软件的安装、更新和移除。

<!--more-->


### 安装软件。

```
# dpkg -i ~/package.deb
```



### 列出与该包关联的文件。

```
# dpkg -L package
```



### 显示软件包的版本。

```
# dpkg -l package
```



### 移除软件包（保留配置）

```
# dpkg -r package
```



### 移除软件包 （不保留配置）

```
# dpkg -P package
```



### 查看软件包的详细信息

```
# dpkg -s package
```



### 列出deb包的内容

```
# dpkg -c package.deb
```



