---
title: Next7.6 主题深度优化
comments: true
tags: next7.6
categories: blog
keywords: 'hexo,next7.6'
description: 基于next7.6版本进行优化
abbrlink: 96933b19
---



## 部署博客

在博客根目录运行下面命令安装插件：

```
npm install hexo-deployer-git --save
```

打开**站点配置文件 `_config.yml`** 并搜索 `deploy:` ,在其中添加下列代码：

```
deploy:
 -
  type: 'git'
  repository: https://github.com/ling218/ling218.github.io.git
  branch: master
```

在博客根目录运行下面命令部署博客：

```
hexo d -g
```



## 文章置顶

在博客根目录运行下面命令安装插件：

```
npm uninstall hexo-generator-index --save
npm install hexo-generator-index-pin-top --save
```

打开`./themes/next/layout/_macro/post.swig`并搜索`<div class="post-meta">`,在其中添加下列代码以开启标签。

```
{% if post.top %}
	<i class="fa fa-thumb-tack"></i>
	<font color=FF0000>置顶</font>
	<span class="post-meta-divider">|</span>
{% endif %}
```

在需要置顶的文章中加入`top: true`即可，如：

```
---
title: hexo
date: 2019-05-02 19:00:00
categories: hexo
top: 100
---
```

**注意：top 中数字越大，文章越靠前**。



## 图片懒加载

只在需要的时候加载图片，而不是一次性加载完成，加快速度，适用于图片较多的博客。

在博客根目录执行命令安装插件：

```
npm install hexo-lazyload --save
```

在**站点配置文件`_config.yml`**中添加下列代码：

```
lazyload:
  enable: true
  onlypost: false #是否只在文章中开启懒加载
  # className: #可选 e.g. .J-lazyload-img
  # loadingImg: #可选 eg. ./images/loading.png 作为图片未加载时的替代图片
```



## 搜索功能

开启站内搜索功能方便快速定位，在博客根目录执行命令安装插件：

```
npm install hexo-generator-searchdb --save
```

打开**主题的配置文件 `_config.yml`** 并搜索 `local_search`，修改为：

```
# Local search
# Dependencies: https://github.com/theme-next/hexo-generator-searchdb
local_search:
  enable: true
  # If auto, trigger search by changing input.
  # If manual, trigger search by pressing enter key or search button.
  trigger: auto
  # Show top n results per article, show all results by setting to -1
  top_n_per_article: 1
  # Unescape html strings to the readable one.
  unescape: false
```



## 顶部阅读进度条

在博文页面顶部添加[Reading Progress](https://github.com/theme-next/theme-next-reading-progress)进度条，表示阅读进度，Next主题已内置配置文件。

切换到Next主题文件夹。

```
cd /themes/next
```

安装模块到`source/lib`文件夹。

```
git clone https://github.com/theme-next/theme-next-reading-progress source/lib/reading_progress
```

打开**主题配置文件`_config.yml`**，搜索`reading_progress`定位到如下代码并将`enable:`设置为`true`：

```
# Reading progress bar
# Dependencies: https://github.com/theme-next/theme-next-reading-progress
reading_progress:
  enable: true
  color: "#ff0000" # 调整线条颜色
  height: 2px # 调整线条高度
```

默认颜色是青色，我修改的是红色。顶部阅读进度条可能会和加载条重合，可以通过修改线条高度覆盖，我的线条高度设置成了`3px`。



## 加载条

用于在网页加载的过程中显示加载进度，Next主题已支持[PACE](https://github.com/theme-next/theme-next-pace)功能。有多种加载动画可选。

切换到Next主题文件夹。

```
cd themes/next
```

安装模块到`source/lib`文件夹。

```
git clone https://github.com/theme-next/theme-next-pace source/lib/pace
```

打开**主题配置文件`_config.yml`**，搜索`pace`定位到如下代码：

```
# Progress bar in the top during page loading.
# Dependencies: https://github.com/theme-next/theme-next-pace
pace: true
# Themes list:
# pace-theme-big-counter | pace-theme-bounce | pace-theme-barber-shop | pace-theme-center-atom
# pace-theme-center-circle | pace-theme-center-radar | pace-theme-center-simple | pace-theme-corner-indicator
# pace-theme-fill-left | pace-theme-flash | pace-theme-loading-bar | pace-theme-mac-osx | pace-theme-minimal
pace_theme: pace-theme-flash
```

将`pace:`设置为`true`
将`pace_theme:`设置为上面`Themes list:`中的一种，主题样式可以在[PACE](https://github.hubspot.com/pace/docs/welcome/)查看。

为了让进度条和主题背景相匹配，可以在`./themes/next/source/lib/pace`中找到相应主题的css文件并修改颜色配置。下面代码以`pace-theme-flash.min.css`示例

```
.pace {
  -webkit-pointer-events: none;
  pointer-events: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.pace-inactive {
  display: none;
}

.pace .pace-progress {
  background: #ffc0cb;
  position: fixed;
  z-index: 2000;
  top: 0;
  right: 100%;
  width: 100%;
  height: 2px;
}

.pace .pace-progress-inner {
  display: block;
  position: absolute;
  right: 0px;
  width: 100px;
  height: 100%;
  box-shadow: 0 0 10px #ffc0cb, 0 0 5px #ffc0cb;
  opacity: 1.0;
  -webkit-transform: rotate(3deg) translate(0px, -4px);
  -moz-transform: rotate(3deg) translate(0px, -4px);
  -ms-transform: rotate(3deg) translate(0px, -4px);
  -o-transform: rotate(3deg) translate(0px, -4px);
  transform: rotate(3deg) translate(0px, -4px);
}

.pace .pace-activity {
  display: block;
  position: fixed;
  z-index: 2000;
  top: 15px;
  right: 15px;
  width: 14px;
  height: 14px;
  border: solid 2px transparent;
  border-top-color: #ffc0cb;
  border-left-color: #ffc0cb;
  border-radius: 10px;
  -webkit-animation: pace-spinner 400ms linear infinite;
  -moz-animation: pace-spinner 400ms linear infinite;
  -ms-animation: pace-spinner 400ms linear infinite;
  -o-animation: pace-spinner 400ms linear infinite;
  animation: pace-spinner 400ms linear infinite;
}

@-webkit-keyframes pace-spinner {
  0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
}
@-moz-keyframes pace-spinner {
  0% { -moz-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -moz-transform: rotate(360deg); transform: rotate(360deg); }
}
@-o-keyframes pace-spinner {
  0% { -o-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -o-transform: rotate(360deg); transform: rotate(360deg); }
}
@-ms-keyframes pace-spinner {
  0% { -ms-transform: rotate(0deg); transform: rotate(0deg); }
  100% { -ms-transform: rotate(360deg); transform: rotate(360deg); }
}
@keyframes pace-spinner {
  0% { transform: rotate(0deg); transform: rotate(0deg); }
  100% { transform: rotate(360deg); transform: rotate(360deg); }
}
```

使用搜索功能搜索`#`,定位到颜色设置，上述代码中一共有5处。



## 鼠标点击特效 （心形特效）

在网页中加入鼠标点击特效，目前有心形和爆炸两种特效，两者可以叠加，特效对性能较低的设备非常不友好，尤其是爆炸特效。

在`./themes/next/source/js`中新建`clicklove.js`，并将粘贴下列代码。

```
!function(e,t,a){function n(){c(".heart{width: 10px;height: 10px;position: fixed;background: #f00;transform: rotate(45deg);-webkit-transform: rotate(45deg);-moz-transform: rotate(45deg);}.heart:after,.heart:before{content: '';width: inherit;height: inherit;background: inherit;border-radius: 50%;-webkit-border-radius: 50%;-moz-border-radius: 50%;position: fixed;}.heart:after{top: -5px;}.heart:before{left: -5px;}"),o(),r()}function r(){for(var e=0;e<d.length;e++)d[e].alpha<=0?(t.body.removeChild(d[e].el),d.splice(e,1)):(d[e].y--,d[e].scale+=.004,d[e].alpha-=.013,d[e].el.style.cssText="left:"+d[e].x+"px;top:"+d[e].y+"px;opacity:"+d[e].alpha+";transform:scale("+d[e].scale+","+d[e].scale+") rotate(45deg);background:"+d[e].color+";z-index:99999");requestAnimationFrame(r)}function o(){var t="function"==typeof e.onclick&&e.onclick;e.onclick=function(e){t&&t(),i(e)}}function i(e){var a=t.createElement("div");a.className="heart",d.push({el:a,x:e.clientX-5,y:e.clientY-5,scale:1,alpha:1,color:s()}),t.body.appendChild(a)}function c(e){var a=t.createElement("style");a.type="text/css";try{a.appendChild(t.createTextNode(e))}catch(t){a.styleSheet.cssText=e}t.getElementsByTagName("head")[0].appendChild(a)}function s(){return"rgb("+~~(255*Math.random())+","+~~(255*Math.random())+","+~~(255*Math.random())+")"}var d=[];e.requestAnimationFrame=function(){return e.requestAnimationFrame||e.webkitRequestAnimationFrame||e.mozRequestAnimationFrame||e.oRequestAnimationFrame||e.msRequestAnimationFrame||function(e){setTimeout(e,1e3/60)}}(),n()}(window,document);
```

打开`./themes/next/layout/_layout.swig`并在末尾添加以下代码。

```
<!-- 页面点击心形 -->
<script type="text/javascript" src="/js/clicklove.js"></script>
```



## 背景及半透明效果

将选好的背景图片放在`博客根目录/themes/next/source/images`中。

打开 **主题配置文件`_config.yml`** 并搜索 `custom_file_path`，修改为：

```
custom_file_path:
  style: source/_data/styles.styl
```

在博客根目录下创建`_data`目录。

```
mkdir -p source/_data
```

在`_data`目录下创建`styles.styl`文件，添加如下内容。

```
body {
    background:url(/images/background.jpg);//图片路径
    background-repeat: no-repeat;//是否重复平铺
    background-attachment:fixed;//是否随着网页上下滚动而滚动，fixed为固定
    background-position:50% 50%;//图片位置
    background-size: cover;//图片展示大小
    -webkit-background-size: cover;
    -o-background-size: cover;
    -moz-background-size: cover;
    -ms-background-size: cover;

     #footer > div > div {
        color:#000000;//底部文字颜色
    }
}

.header-inner {
    background: #ffffffe8;//设置侧边栏透明
    opacity: 1;//这里设置为1，否则搜索栏会消失
}
.main-inner {
		background: #fff;
		opacity: 0.9;//设置文章主体透明度
}
```

背景图片不要太大，最好压缩一下，太大网站加载速度会极慢。

透明度不要调太低，影响阅读。



## 在文章结尾添加本文结束标志

打开 **主题配置文件`_config.yml`** 并搜索 `custom_file_path`，修改为：

```
custom_file_path:
  postBodyEnd: source/_data/post-body-end.swig
```

在`_data`目录下创建`post-body-end.swig`文件，添加如下内容。

```
<div>
    {% if not is_index %}
        <div style="text-align:center;color: #ccc;font-size:14px;">-------------本文结束<i class="fa fa-paw"></i>感谢您的阅读-------------</div>
    {% endif %}
</div>
```



## 修改代码块颜色

打开`source/_data/styles.styl`文件并添加下列代码，`color`可以自定义设置。

```
// ``代码块样式
code {
    color: #ff0000;
    background: #d1d1d1d1;
    border-radius: 3px;
}

// 文章```代码块顶部样式
.highlight figcaption {
    margin: 0em;
    padding: 0.5em;
    background: #eee;
    border-bottom: 1px solid #ff0000;
}
.highlight figcaption a {
    color: rgb(80, 115, 184);
}                 
```



## 网站运行时间

打开`./themes/next/layout/_partials/footer.swig`并添加下列代码。

```
<div>
<span id="timeDate">载入天数...</span><span id="times">载入时分秒...</span>
<script>
    var now = new Date();
    function createtime() {
        var grt= new Date("03/31/2019 00:00:00");
        now.setTime(now.getTime()+250);
        days = (now - grt ) / 1000 / 60 / 60 / 24; dnum = Math.floor(days);
        hours = (now - grt ) / 1000 / 60 / 60 - (24 * dnum); hnum = Math.floor(hours);
        if(String(hnum).length ==1 ){hnum = "0" + hnum;} minutes = (now - grt ) / 1000 /60 - (24 * 60 * dnum) - (60 * hnum);
        mnum = Math.floor(minutes); if(String(mnum).length ==1 ){mnum = "0" + mnum;}
        seconds = (now - grt ) / 1000 - (24 * 60 * 60 * dnum) - (60 * 60 * hnum) - (60 * mnum);
        snum = Math.round(seconds); if(String(snum).length ==1 ){snum = "0" + snum;}
        document.getElementById("timeDate").innerHTML = "本站已安全运行 "+dnum+" 天 ";
        document.getElementById("times").innerHTML = hnum + " 小时 " + mnum + " 分 " + snum + " 秒";
    }
setInterval("createtime()",250);
</script>
</div>
```



## 永久链接

在博客根目录下运行下面命令安装永久链接插件：

```
npm install hexo-abbrlink --save
```

在**站点配置文件`_config.yml`** 中，修改：

```
url: https://tding.top/
root: /
permalink: archives/:abbrlink.html
abbrlink:
  alg: crc32  # 算法：crc16(default) and crc32
  rep: hex    # 进制：dec(default) and hex
permalink_defaults:
```





## nofollow 标签

减少出站链接能够有效防止权重分散，hexo 有很方便的自动为出站链接添加 `nofollow` 标签的插件。

在博客根目录下运行下面命令安装nofollow标签插件：

```
npm install hexo-filter-nofollow --save
```

在**站点配置文件`_config.yml`** 中添加配置，将 `nofollow` 设置为 `true`：

```
# 外部链接优化
nofollow:
  enable: true
  field: site
  exclude:
    - 'blog.ling218.cn'
    - 'exclude2.com'
```

其中 `exclude`（例外的链接，比如友链）将不会被加上 `nofollow` 属性。



## 404页面

在 `hexo/source/404/` 目录下创建自己的 `index.md`：

```
---
title: 404
date: 2019-12-06 23:59:37
type: "404"
comments: false
permalink: /404
---


**对不起，您所访问的页面不存在或者已删除**
你可以**[点击此处](https://www.ling218.cn)**返回首页。


* 我的Github：[http://github.com/ling218](http://github.com/ling218)
* 我的Facebook：[https://www.facebook.com/suixin911](https://www.facebook.com/suixin911)
```

这样我们就完成了 404 页面的创建。

注意：本地测试不出来，发布出来就可以了。



## RSS订阅

在博客根目录下运行下面命令安装RSS订阅插件：

```
npm install hexo-generator-feed --save
```

在**主题配置文件`_config.yml`** 中搜索`social` 并添加以下代码：

```
social:
	RSS: /atom.xml || rss
```



## footer 跳动的红心

在**主题配置文件 `_config.yml`**中搜索 `footer`  并修改以下代码：

```
footer:
	icon:
	    name: heartbeat     #图标样式
	    animated: true      #图标是否跳动
	    color: "#ff0000"    #图标颜色
```



## 首页文章属性

在**主题配置文件 `_config.yml`**中搜索 `post_mate:`  并修改以下代码：

```
post_meta:
  item_text: false    # 设为true 可以一行显示，文章的所有属性
  created_at: true    # 显示创建时间
  updated_at:
    enabled: true     # 显示修改的时间
    another_day: true # 设true时，如果创建时间和修改时间一样则显示一个时间
  categories: true    # 显示分类信息
```



## 页面阅读统计

在**主题配置文件 `_config.yml`**中搜索 `busuanzi_count:`  并修改以下代码：

```
busuanzi_count:
  enable: false              # 设true 开启
  total_visitors: true       # 总阅读人数（uv数）
  total_visitors_icon: user  # 阅读总人数的图标
  total_views: true          # 总阅读次数（pv数）
  total_views_icon: eye      # 阅读总次数的图标
  post_views: true           # 开启内容阅读次数
  post_views_icon: eye       # 内容页阅读数的图标
```



## 字数统计、阅读时长

在博客根目录下运行下面命令安装统计插件：

```
npm install hexo-symbols-count-time --save
```

在**主题配置文件`_config.yml`** 搜索 `symbols_count_time:` 并修改如下：

```
symbols_count_time:
  separated_meta: true  # false会显示一行
  item_text_post: true  # 显示属性名称,设为false后只显示图标和统计数字,不显示属性的文字
  item_text_total: true # 底部footer是否显示字数统计属性文字
  awl: 4                # 计算字数的一个设置,没设置过
  wpm: 275              # 一分钟阅读的字数
```

在**站点配置文件`_config.yml`** 新增如下内容：

```
symbols_count_time:
 #文章内是否显示
  symbols: true
  time: true
 # 网页底部是否显示
  total_symbols: true
  total_time: true
```



## LeanCloud 安全

在博客根目录下运行下面命令安装leancloud安全插件：

```
npm install hexo-leancloud-counter-security --save
```

在**主题配置文件`_config.yml`** 搜索 `leancloud_visitors` 并修改如下：

```
leancloud_visitors:
  enable: true
  app_id: <<your app id>>
  app_key: <<your app key>>
  # Dependencies: https://github.com/theme-next/hexo-leancloud-counter-security
  security: true
  betterPerformance: false
```

> 对 betterPerformance 选项的说明：由于 Leancloud 免费版的云引擎存在请求线程数和运行时间限制以及休眠机制，很多时候访客数量加载会很慢。如果设置 betterPerformance 为 true，则网页则会在提交请求之前直接显示访客人数为查询到的人数 + 1，以增加用户体验。

在**站点配置文件`_config.yml`** 新增如下内容：

```
leancloud_counter_security:
  enable_sync: true
  app_id: <<your app id>>
  app_key: <<your app key>
  username: <<username>>  #如留空则将在部署时询问
  password: <<password>>  #建议留空以保证安全性，如留空则将在部署时询问
```

在博客根目录下运行下面命令：

```
hexo lc-counter register <<username>> <<password>>
```

将 `<username>` 和 `<password>` 替换为你自己的用户名和密码（不必与 leancloud 的账号）相同。此用户名和密码将在 hexo 部署时使用。

在**站点配置文件`_config.yml`** 的 deploy 下新增如下内容：

```
deploy:
  # other deployer
- type: leancloud_counter_security_sync
```



## 微信，支付宝打赏

在**主题配置文件`_config.yml`** 搜索 `reward_comment:` 并修改如下：

```
# Reward
reward_comment:                   # 打赏描述
wechatpay: /images/wechatpay.png  # 微信支付的二维码图片地址
alipay: /images/alipay.png        # 支付宝的地址
#bitcoin: /images/bitcoin.png     # 比特币地址
```



## 相关文章推荐

在博客根目录下运行下面命令安装相关文章推荐插件：

```
npm install hexo-related-popular-posts --save
```

在**主题配置文件 `_config.yml`** 搜索 `related_posts` 并修改如下内容:

```
related_posts:
  enable: true
  title: 相关文章推荐      # 属性的命名
  display_in_home: false # false代表首页不显示
  params:
    maxCount: 5          # 最多5条
    #PPMixingRate: 0.0   # 相关度
    #isDate: true        # 是否显示日期
    #isImage: false      # 是否显示配图
    isExcerpt: false     # 是否显示摘要
```



## 代码块复制按钮

在**主题配置文件`_config.yml`** 搜索 `codeblock:` 并修改如下：

```
codeblock:
  copy_button:
    enable: false      # 增加复制按钮的开关
    show_result: false # 点击复制完后是否显示 复制成功 结果提示
```



## 版权信息

在**主题配置文件`_config.yml`** 搜索 `creative_commons:` 并修改如下：

```
creative_commons:
  license: by-nc-sa
  sidebar: false
  post: true       # 默认显示版权信息
  language:
```



## 豆瓣读书、豆瓣电影插件

在博客根目录下运行下面命令安装豆瓣读书、豆瓣电影插件：

```
npm install hexo-douban --save
```

在**站点配置文件`_config.yml`** 新增如下内容：

```
douban:
  user: mythsman
  builtin: false
  book:
    title: 'This is my book title'
    quote: 'This is my book quote'
  movie:
    title: 'This is my movie title'
    quote: 'This is my movie quote'
  game:
    title: 'This is my game title'
    quote: 'This is my game quote'
  timeout: 10000
```

- user: 你的豆瓣 ID. 打开豆瓣，登入账户，然后在右上角点击 “个人主页” ，这时候地址栏的 URL 大概是这样：”`https://www.douban.com/people/xxxxxx/`“，其中的”xxxxxx” 就是你的个人 ID 了。
- builtin: 是否将生成页面的功能嵌入 `hexo s` 和 `hexo g` 中，默认是 false, 另一可选项为 true (1.x.x 版本新增配置项)。
- title: 该页面的标题。
- quote: 写在页面开头的一段话，支持 html 语法。
- timeout: 爬取数据的超时时间，默认是 10000ms , 如果在使用时发现报了超时的错 (ETIMEOUT) 可以把这个数据设置的大一点。

如果只想显示某一个页面 (比如 movie)，那就把其他的配置项注释掉即可。

我们只需要在终端中输入以下命令：`hexo clean && hexo douban -bgm && hexo g && hexo s` 即可，注意其中开启 hexo-douban 的命令中，-bgm 代表的是 book、game、movie 三个参数，如果只需要其中的一部分就只带你想要的那些参数。

**注意：由于 hexo douban 的简写也是 hexo d，与 hexo deploy 的简写指令 hexo d 冲突，因此在进行二者部署的时候，只能都打全名而不能打简写形式**。

- 在 0.x.x 版本中，文章的更新和豆瓣页面的爬取操作是绑定在一起的，无法支持单独更新文章或者单独爬取文章。
- 在 1.x.x 版本中，使用 hexo douban 命令即可生成指定页面，如果不加参数，那么默认参数为 - bgm。如果配置了 builtin 参数为 true，那么除了可以使用 hexo douban 命令之外，hexo g 或 hexo s 也内嵌了生成页面的功能。因此，还是按照 `hexo clean && hexo g && hexo s` 就可以。

上面都没问题之后，我们只需要在站点目录下测试 `http://localhost:4000/books` 或者 `http://localhost:4000/movies` 等，如果看到页面了就说明成功了。

如果上述都没有问题，我们就可以在菜单栏中添加按钮了，打开主题配置文件`_config.yml`，找到菜单按钮，添加下面内容：

```
menu:
  movies: /movies/ || film
  books: /books/ || book
```

然后在 `next/language/zh-CN.yml` 文件中添加对应的中文参数信息：

```
menu:
  movies: 观影
  books: 阅读
```



## 添加 Google 统计

访问 Google Analytics，按照提示填写网站信息开通 GA 服务，获取统计 ID。

在**主题配置文件`_config.yml`**搜索 `google_analytics`，删除注释`#`并填写获取到的统计 ID：

```
# Google Analytics
google_analytics:
  tracking_id: 
  localhost_ignored: true
```



## 站点地图

### 通用站点地图

在博客根目录下运行下面命令安装sitemap插件：

```
npm install hexo-generator-sitemap --save
```

在**站点配置文件`_config.yml`** 新增如下内容：

```
# 通用站点地图
sitemap:
  path: sitemap.xml
```



### 百度站点地图

在博客根目录下运行下面命令安装baidu-sitemap插件:

```
npm install hexo-generator-baidu-sitemap --save
```

在**站点配置文件`_config.yml`** 新增如下内容：

```
# 百度站点地图
baidusitemap:
  path: baidusitemap.xml
```



### 百度主动推送

在博客根目录下运行下面命令安装baidu主动推送插件:

```
npm install hexo-baidu-url-submit --save
```

在**站点配置文件`_config.yml`** 新增如下内容：

```
# 百度主动推送
baidu_url_submit:
  count: 5 				     ## 提交最新的1个链接
  host: tding.top 	     ## 百度站长平台中注册的域名
  token: 	 ## 准入秘钥
  path: baidu_urls.txt 		 ## 文本文档的地址， 新链接会保存在此文本文档里
```



