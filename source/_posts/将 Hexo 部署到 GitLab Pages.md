---
title: 将 Hexo 部署到 GitLab Pages
comments: true
tags: gitlab
categories: blog
keywords: 'hexo,gitlab'
description: GitHub Pages 屏蔽了百度爬虫，GitLab Pages 可以让百度爬虫抓取。
abbrlink: 4971f44e
---



```
mkdir work
cd work
npm install hexo-cli -g
hexo init
hexo install
hexo s
git clone https://github.com/theme-next/hexo-theme-next themes/next
rm -rf themes/next/.git/
git init
git remote add origin https://gitlab.com/ling218/ling218.gitlab.io.git
git add .
git commit -m "Update"
git push -u origin master
echo "lab.ling218.cn" > source/CNAME
cat <<-EOF >.gitlab-ci.yml
image: node:11.15.0
cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - npm install

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
EOF

```