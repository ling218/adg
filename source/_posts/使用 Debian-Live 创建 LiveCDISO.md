---
title: 使用 Debian-Live 创建 LiveCD
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,linux,debian'
description: 创建 LiveCD
abbrlink: 9169e7b2
---



## 操作环境

- **Debian10 (Buster)**



## 添加中文源

```
# cat << EOF > /etc/apt/sources.list
deb http://mirrors.ustc.edu.cn/debian/ buster main contrib non-free
deb http://mirrors.aliyun.com/debain/ buster main contrib non-free
deb http://mirrors.163.com/debian/ buster main contrib non-free
EOF
```



## 安装构建环境的应用程序

```
# apt update
# apt install \
	live-build \
	debootstrap \
	xorriso \
	squashfs-tools 
```



## 创建最小化的LiveCD

```
# mkdir livecd && cd livecd
# lb clean
# lb build
```



## 创建带有救援工具的LiveCD

```
# mkdir livecd && cd livecd
# lb clean
# lb config --bootappend-live "boot=live components locales=zh_CN.UTF-8 keyboard-layouts=zh"
# echo "aptitude gdebi mc less testdisk ntfs-3g exfat-fuse exfat-utils parted parted-doc links2 gddrescue memtester nullmailer smartmontools lm-sensors" >> config/package-lists/terminal-rescue.list.chroot
# lb build
```



## 创建带有XFCE桌面的LiveCD

```
# lb clean
# lb config --bootappend-live "boot=live components locales=zh_CN.UTF-8 keyboard-layouts=zh"
# echo "task-german-desktop task-xfce-desktop aptitude bleachbit bluez bluez-hcidump bluez-tools python-bluez bluez hwinfo hwloc libhwloc-plugins libhwloc-doc linux-cpupower gdebi hdparm hotswap hotswap-gui hotswap-text parted parted-doc gparted safecopy testdisk virt-manager wammu distro-info osinfo-db-tools emacs jedit libreoffice libreoffice-l10n-de libreoffice-help-de libreoffice-pdfimport lyx childsplay childsplay-alphabet-sounds-de pysolfc pysolfc-cardsets etherape simple-scan nautilus rhythmbox digikam digikam-doc gtkam gtkam-gimp gimp gimp-data-extras gimp-dds gimp-gap gimp-gluas gimp-gmic gimp-gutenprint gimp-lensfun gimp-plugin-registry gimp-texturize gimp-ufraw gimp-help-de gimp-help-en gwenview inkscape krita krita-l10n okular okular-backend-odp okular-backend-odt okular-extra-backends kdiff3 kdiff3-doc konsole nullmailer icedove icedove-l10n-de hwdata isenkram inxi undistract-me ethstats ethstatus ethtool gftp hashcat macchanger qbittorrent rsync grsync pidgin uget wicd wicd-gtk wifi-radar dosbox dvdisaster dvdisaster-doc exfat-fuse exfat-utils isomaster ntfs-3g screen screenie alsa-tools alsa-tools-gui kile kile-doc kile-l10n less clamav clamdscan clamtk doublecmd-gtk doublecmd-plugins gddrescue ddrescueview locate mbw mc memtester gscan2pdf hddtemp lm-sensors smartmontools smart-notifier gsmartcontrol ultracopier unison-gtk handbrake handbrake-cli recordmydesktop gtk-recordmydesktop vlc vlc-l10n chromium chromium-l10n firefox-esr firefox-esr-l10n-de firefox-esr-ublock-origin firefox-esr-toggle-proxy firefox-esr-useragentswitcher firefox-esr-webdeveloper firefox-esr-noscript firefox-esr-https-everywhere firefox-esr-downthemall links2 privoxy wireless-tools wireshark guake mesa-utils mesa-utils-extra terminator totem kdenlive openshot simplescreenrecorder smplayer nload iftop iptraf-ng nethogs bmon slurm tcptrack vnstat bwm-ng cbm speedometer pktstat ifstat" >> config/package-lists/xfce-desktop.list.chroot
# lb build
```



## 创建带有XFCE桌面的LiveCD

```
# lb clean
# lb config --bootappend-live "boot=live components locales=zh_CN.UTF-8 keyboard-layouts=zh"
# echo "task-german-desktop task-xfce-desktop games-finest-light" >> config/package-lists/xfce-desktop-games-finest-light.list.chroot
# lb build
```

