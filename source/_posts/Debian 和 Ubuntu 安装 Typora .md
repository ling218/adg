---
title: Debian/Ubuntu 安装 Typora
comments: true
tags: typora
categories: linux
keywords: 'linux,typora'
description: markdown 编辑器
abbrlink: e3fe7203
---

## 安装Typora

```
# 添加typora仓库密钥
# sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BA300B7755AFCFAE
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -

# 把typora软件源添加到系统源文件 
echo -e "\ndeb https://typora.io/linux ./" | sudo tee -a /etc/apt/sources.list
sudo apt-get update

# 安装typora
sudo apt-get install typora
```

## 升级Typora

安装Typora之后，`typora`软件包将受到管理`apt-get`，因此当您的系统更新已安装的软件包或执行时`apt-get upgrade`，Typora将更新为最新版本。

```
sudo apt-get upgrade
```

