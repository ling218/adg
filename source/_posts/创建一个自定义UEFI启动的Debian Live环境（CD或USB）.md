---
title: 创建一个自定义 UEFI 启动的 Debian Live 环境 （CD或USB）
comments: true
tags: live-build
categories: live-build
keywords: live build
description: 创建一个自定义UEFI启动的Debian Live环境
abbrlink: 1a14b41f
---


这些是我在**Ubuntu 18.04 LTS（Bionic Beaver）** 64位系统上使用的步骤，用于构建可以从CD或USB引导的**x86 Debian 9（Stretch）**实时环境。

这些步骤可用于创建可启动BIOS（MBR），可启动UEFI（GPT）或可启动UEFI和BIOS的组合的实时环境。本指南的独特之处在于**未使用Syslinux / Isolinux。只有Grub引导设备。**这样做是为了保持一致性，并避免两者混淆（仅Syslinux / Isolinux不能完成本文中介绍的所有内容，但是Grub可以做到）。

以下是我的指南的替代方法，对于那些阅读本文的人来说，它们可能是更好的解决方案：[live-build](https://manpages.debian.org/jessie/live-build/live-build.7.en.html)，[mkusb](https://help.ubuntu.com/community/mkusb)，[UNetbootin](https://unetbootin.github.io/)，[xixer](https://github.com/jnalley/xixer)，[rufus](https://rufus.akeo.ie/)，[YUMI](https://www.pendrivelinux.com/yumi-multiboot-usb-creator/)，[Simple-cdd](https://wiki.debian.org/Simple-CDD)。您还应该查看[Debian DebianCustomCD文档，](https://wiki.debian.org/DebianCustomCD)因为它将比本文提供的信息多得多。

我写这本指南的目的更多是出于教育目的。它不一定是满足您需求的最快指南或最佳指南。希望对您有所帮助。

**警告**：我已**突出显示**您应该在 **[chroot]**环境中的所有位置。小心！在本地环境而不是[ chroot中](https://en.wikipedia.org/wiki/Chroot)运行其中一些命令可能会损坏系统。

# 先决条件

安装我们需要构建环境的应用程序。

```
sudo apt-get install \
    debootstrap \
    squashfs-tools \
    xorriso \
    grub-pc-bin \
    grub-efi-amd64-bin \
    mtools
```

创建一个目录，我们将在其中存储在本指南中创建的所有文件。

```
mkdir -p $HOME/LIVE_BOOT
```

# 引导和配置Debian

设置基本的Debian环境。我正在使用`stretch`我的发行版和`i386`体系结构。请查阅[debian镜像](https://www.debian.org/mirror/list)列表。

*如果附近有镜子，请在此命令中更改URL。*

```
sudo debootstrap \
    --arch=i386 \
    --variant=minbase \
    stretch \
    $HOME/LIVE_BOOT/chroot \
    http://ftp.us.debian.org/debian/
```

Chroot到我们刚刚引导的Debian环境。

```
sudo chroot $HOME/LIVE_BOOT/chroot
```

**[chroot]**为您的Debian环境设置自定义主机名。

```
echo "debian-live" > /etc/hostname
```

**[chroot]**确定实时环境中需要哪个Linux内核。

```
apt-cache search linux-image
```

**[chroot]**我选择了图像`linux-image-686`。我也认为这`live-boot`是必要的。`systemd-sys`（或等效项）也是提供init所必需的。

```
apt-get update && \
apt-get install --no-install-recommends \
    linux-image-686 \
    live-boot \
    systemd-sysv
```

**[chroot]**安装您选择的程序，然后运行`apt-get clean`以节省一些空间。我使用`--no-install-recommends`以避免多余的程序包。您应该确定环境所需的内容。

阅读Debian的[ReduceDebian文章，](https://wiki.debian.org/ReduceDebian)以获取有关减小Debian环境大小的技巧（如果大小很重要并且您需要最小和紧凑的安装）。请注意，某些实时环境（如[Tiny Core Linux](https://en.wikipedia.org/wiki/Tiny_Core_Linux)或[Puppy Linux）](https://en.wikipedia.org/wiki/Puppy_Linux)经过专门优化，仅占很小的空间。尽管本文提供了一个相对较小的实时环境，但生成仅几十MB大小的环境仍需要付出额外的精力，而本文并未涉及。

```
apt-get install --no-install-recommends \
    network-manager net-tools wireless-tools wpagui \
    curl openssh-client \
    blackbox xserver-xorg-core xserver-xorg xinit xterm \
    nano && \
apt-get clean
```

**[chroot]**设置root密码。`root`将默认为该实时环境中的唯一用户，但是您可以根据需要添加其他用户。

```
passwd root
```

**[chroot]**退出chroot。

```
exit
```

创建目录，其中将包含我们的实时环境文件和暂存文件的文件。

```
mkdir -p $HOME/LIVE_BOOT/{scratch,image/live}
```

将chroot环境压缩到一个Squash文件系统中。

```
sudo mksquashfs \
    $HOME/LIVE_BOOT/chroot \
    $HOME/LIVE_BOOT/image/live/filesystem.squashfs \
    -e boot
```

将内核和initramfs从中复制`chroot`到`live`目录中。

```
cp $HOME/LIVE_BOOT/chroot/boot/vmlinuz-* \
    $HOME/LIVE_BOOT/image/vmlinuz && \
cp $HOME/LIVE_BOOT/chroot/boot/initrd.img-* \
    $HOME/LIVE_BOOT/image/initrd
```

为grub创建菜单配置文件。请注意，`insmod all_video`在我的测试中需要该行来处理我的一台计算机的[UEFI引导中的错误](https://askubuntu.com/a/857008/413290)。也许不是每个人都需要那条线，但我确实需要。

此配置指示Grub使用该`search`命令来推断哪个设备包含我们的实时环境。考虑到我们可以将实时环境写入可启动媒体的各种方式，这似乎是最可移植的解决方案。

```
cat <<'EOF' >$HOME/LIVE_BOOT/scratch/grub.cfg

insmod all_video

search --set=root --file /DEBIAN_CUSTOM

set default="0"
set timeout=30

menuentry "Debian Live" {
    linux /vmlinuz boot=live quiet nomodeset
    initrd /initrd
}
EOF
```

在`image`named中创建一个特殊文件`DEBIAN_CUSTOM`。该文件将用于帮助`Grub`确定哪个设备包含我们的实时文件系统。该文件名必须唯一，并且必须与我们的`grub.cfg`配置中的文件名匹配。

```
touch $HOME/LIVE_BOOT/image/DEBIAN_CUSTOM
```

您的`LIVE_BOOT`目录现在应该大致如下所示。

```
LIVE_BOOT/chroot/*tons of chroot files*
LIVE_BOOT/scratch/grub.cfg
LIVE_BOOT/image/DEBIAN_CUSTOM
LIVE_BOOT/image/initrd
LIVE_BOOT/image/vmlinuz
LIVE_BOOT/image/live/filesystem.squashfs
```

# 创建可启动媒体

请注意，下面有两组**单独**的说明，用于为实时环境创建可启动媒体。一个进程名为[Create Bootable ISO / CD](https://willhaley.com/blog/custom-debian-live-environment/#create-bootable-isocd)，**另一个进程**名为[Create Bootable USB](https://willhaley.com/blog/custom-debian-live-environment/#create-bootable-usb)。

- 在**创建可启动ISO / CD**指令将导致`.iso`包含我们的生活环境中的图像文件。
- “ **创建可启动USB”**说明将导致我们的实时环境直接安装到USB设备上。

`.iso`我们使用**创建可启动ISO / CD**指令**创建**的文件可以刻录到CD-ROM（光学介质），或通过写入USB设备`dd`。允许我们`.iso`文件中这种“应收帐款”行为的功能*并非*免费提供。这个过程有点复杂，但是在许多现代的实时环境（例如Ubuntu安装`.iso`文件）中，这种行为是很常见的。

*请注意，将`.iso`文件写入USB设备与将实时环境直接安装到USB设备并不相同。在我的[笔记](https://willhaley.com/blog/custom-debian-live-environment/#notes)中详细了解我的发现。*

## 创建可启动的ISO / CD

将实时环境安装到`.iso`可以刻录到光学介质的文件中。

如上所述，`.iso`通过这些步骤生成的文件**可以**使用写入到USB设备`dd`。

创建一个grub UEFI映像。

```
grub-mkstandalone \
    --format=x86_64-efi \
    --output=$HOME/LIVE_BOOT/scratch/bootx64.efi \
    --locales="" \
    --fonts="" \
    "boot/grub/grub.cfg=$HOME/LIVE_BOOT/scratch/grub.cfg"
```

创建包含EFI引导加载程序的FAT16 UEFI引导磁盘映像。请注意，使用`mmd`和`mcopy`命令来复制名为的UEFI引导加载程序`bootx64.efi`。

```
(cd $HOME/LIVE_BOOT/scratch && \
    dd if=/dev/zero of=efiboot.img bs=1M count=10 && \
    mkfs.vfat efiboot.img && \
    mmd -i efiboot.img efi efi/boot && \
    mcopy -i efiboot.img ./bootx64.efi ::efi/boot/
)
```

生成ISO文件。

```
xorriso \
    -as mkisofs \
    -iso-level 3 \
    -full-iso9660-filenames \
    -volid "DEBIAN_CUSTOM" \
    -eltorito-alt-boot \
        -e EFI/efiboot.img \
        -no-emul-boot \
    -append_partition 2 0xef ${HOME}/LIVE_BOOT/scratch/efiboot.img \
    -output "${HOME}/LIVE_BOOT/debian-custom.iso" \
    -graft-points \
        "${HOME}/LIVE_BOOT/image" \
        /EFI/efiboot.img=$HOME/LIVE_BOOT/scratch/efiboot.img
```

现在将ISO刻录到CD，您应该可以使用UEFI系统从CD引导了。

## 创建可启动USB

将实时环境安装到USB设备。

如上所述，在安装现场环境到USB设备是**不**一样的书面`.iso`文件到USB设备。在这两种情况下，最终结果在大多数情况下都是相同的，但是存在一些细微的差异值得理解，并且有正当的理由，有人可能希望将实时环境直接安装到USB设备，而不是将`.iso`文件写入USB设备。 

我假设您在**/ dev /** sdz上有一个已挂载的**空白** USB驱动器。为了在实际的块设备中轻松进行交换，我在这些命令中使用了一个变量。`$disk`

导出`disk`变量。

```
export disk=/dev/sdz
```

为USB驱动器创建一些安装点。

```
sudo mkdir -p /mnt/{usb,efi}
```

使用分割USB驱动器`parted`。此命令在GPT（Guid分区表）布局中创建2个分区。一个分区用于UEFI，另一个分区用于我们的Debian OS和其他实时数据。

```
sudo parted --script $disk \
    mklabel gpt \
    mkpart ESP fat32 1MiB 200MiB \
        name 1 EFI \
        set 1 esp on \
    mkpart primary fat32 200MiB 100% \
        name 2 LINUX \
        set 2 msftdata on
```

格式化UEFI和数据分区。

```
sudo mkfs.vfat -F32 ${disk}1 && \
sudo mkfs.vfat -F32 ${disk}2
```

挂载分区。

```
sudo mount ${disk}1 /mnt/efi && \
sudo mount ${disk}2 /mnt/usb
```

安装用于x86_64 UEFI引导的Grub。

```
sudo grub-install \
    --target=x86_64-efi \
    --efi-directory=/mnt/efi \
    --boot-directory=/mnt/usb/boot \
    --removable \
    --recheck
```

`live`在USB设备上创建目录。

```
sudo mkdir -p /mnt/usb/{boot/grub,live}
```

将我们先前生成的Debian实时环境文件复制到USB磁盘。

```
sudo cp -r $HOME/LIVE_BOOT/image/* /mnt/usb/
```

将`grub.cfg`启动配置复制到USB设备。

```
sudo cp \
    $HOME/LIVE_BOOT/scratch/grub.cfg \
    /mnt/usb/boot/grub/grub.cfg
```

现在卸载磁盘，您应该已经准备好在UEFI系统上从磁盘启动。

```
sudo umount /mnt/{usb,efi}
```

# 注意事项 +

# 注释（Rufus支持） +

# 引文

- [sgdisk（8）-Linux手册页](https://linux.die.net/man/8/sgdisk)
- [混合UEFI GPT + BIOS GPT / MBR引导](https://wiki.archlinux.org/index.php/Multiboot_USB_drive#Hybrid_UEFI_GPT_.2B_BIOS_GPT.2FMBR_boot)
- [“找不到合适的模式”错误](https://wiki.archlinux.org/index.php/GRUB#.22No_suitable_mode_found.22_error)
- [Ubuntu 64位实时CD上的memtest选项在哪里？](https://askubuntu.com/questions/258991/where-is-the-memtest-option-on-the-ubuntu-64-bit-live-cd)
- [重新掌握安装ISO](https://wiki.archlinux.org/index.php/Remastering_the_Install_ISO)
- [在（U）EFI系统中安装GRUB2](https://help.ubuntu.com/community/UEFIBooting#Install_GRUB2_in_.28U.29EFI_systems)
- [6.4将配置文件嵌入GRUB](https://www.gnu.org/software/grub/manual/grub/html_node/Embedded-configuration.html)
- [独立的Grub2 EFI安装-grub.cfg放置？](https://askubuntu.com/questions/643938/standalone-grub2-efi-installation-grub-cfg-placement)
- [“ grub-mkimage –config =”的实际用法](https://unix.stackexchange.com/questions/253657/actual-usage-of-grub-mkimage-config)
- [GRUB2使用方法（4）：内存磁盘和回送设备](http://lukeluo.blogspot.com/2013/06/grub-how-to-4-memdisk-and-loopback.html)
- [grub efi加载程序如何找到正确的grub.cfg和引导目录？](https://unix.stackexchange.com/questions/267765/how-does-the-grub-efi-loader-find-the-correct-grub-cfg-and-boot-directory)
- [如何在Linux上救援非引导式GRUB 2](https://www.linux.com/learn/how-rescue-non-booting-grub-2-Linux)
- [使用光盘](https://help.ubuntu.com/community/BootFromUSB#Using_a_CD)
- [从EFI和GPT用extlinux引导Linux](https://superuser.com/questions/746553/boot-linux-with-extlinux-from-efi-gpt)
- [GRUB2模块](https://blog.fpmurphy.com/2010/06/grub2-modules.html)
- [grub-install.c](https://github.com/coreos/grub/blob/master/util/grub-install.c)
- [Grub2 /问题排查](https://help.ubuntu.com/community/Grub2/Troubleshooting)
- [设置预加载器](https://wiki.archlinux.org/index.php/Secure_Boot#Set_up_PreLoader)
- [使用PreLoader](https://wiki.archlinux.org/index.php/REFInd#Using_PreLoader)
- [build.sh](https://github.com/Mic92/archlive/blob/master/build.sh#L138)
- [阿奇索](https://wiki.archlinux.org/index.php/archiso#Installing_packages_from_multilib)
- [简单的菜单系统](https://www.syslinux.org/wiki/index.php?title=Menu#The_simple_menu_system)
- [UEFI](https://www.syslinux.org/wiki/index.php?title=Isohybrid#UEFI)
- [制作UEFI可启动实时CD](https://www.linuxquestions.org/questions/linux-general-1/make-uefi-bootable-live-cd-926021/)
- [将ISOLINUX与UEFI一起使用的正确方法是什么？](https://unix.stackexchange.com/questions/285193/what-is-the-proper-way-to-use-isolinux-with-uefi)
- [从可移动媒体引导](https://wiki.debian.org/UEFI#Booting_from_removable_media)
- [UEFI系统](https://wiki.archlinux.org/index.php/GRUB#UEFI_systems)
- [管理Linux的EFI引导加载程序：EFI引导加载程序安装](https://www.rodsbooks.com/efi-bootloaders/installation.html)
- [例子](https://wiki.archlinux.org/index.php/syslinux#Boot_prompt)
- [Grub2 El-Torito CD](https://forum.osdev.org/viewtopic.php?t=22169&p=178135)
- [使用GRUB2创建ISO映像](https://forum.osdev.org/viewtopic.php?f=1&t=23766)
- [DebianLive MultibootISO](https://wiki.debian.org/DebianLive/MultibootISO)
- [11个GRUB图像文件](https://www.gnu.org/software/grub/manual/grub/html_node/Images.html)
- [stage2_eltorito遗失](https://www.linuxquestions.org/questions/linux-software-2/stage2_eltorito-missing-884944/)
- [3.4制作GRUB可引导CD-ROM](https://www.gnu.org/software/grub/manual/legacy/Making-a-GRUB-bootable-CD_002dROM.html)
- [从CD上的el torito引导时，grub2为什么会忽略内核选项？](https://unix.stackexchange.com/questions/283994/why-is-grub2-ignoring-kernel-options-when-boot-from-el-torito-on-cd)
- [从CD / DVD引导UEFI无效](https://communities.vmware.com/message/2228281#2228281)
- [升级到VMWS Player 12后的UEFI问题](https://communities.vmware.com/message/2583742#2583742)
- [品牌](https://github.com/linuxkit/linuxkit/blob/master/tools/mkimage-iso-efi/make-efi)
- [UEFI + BIOS可启动实时Debian Stretch amd64持久化](https://unix.stackexchange.com/questions/382817/uefi-bios-bootable-live-debian-stretch-amd64-with-persistence)
- [[syslinux\] Isohybrid Wiki页面和UEFI](https://www.syslinux.org/archives/2015-April/023381.html)

## 原文地址：

- https://willhaley.com/blog/custom-debian-live-environment/

