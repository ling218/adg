---
title: GitHub Build-live 项目收集
comments: true
tags: URL
categories: live-build
keywords: 'linux,live-build'
description: GitHub Build-live 项目收集
abbrlink: 3311a575
---



### 创建 Ubuntu live 脚本

```
git clone https://github.com/RTXI/live-image
```



### 构建 Debain live 脚本 （基于 debian live）

```
git clone https://github.com/s-vincent/debian-live-image
```



### 构建 Debain live 脚本 (分步骤小脚本)

```
git clone https://github.com/dinooz/devuanos
```



### 构建 NIIX-LINUX (中文项目，项目完整度高)

```
git clone https://github.com/etony/niix-linux
```



### 构建 Debian live （非常完整的项目，没成功构建过）

```
git clone https://github.com/nodiscc/debian-live-config
```



### 构建 Debian live （非常完整的项目）

```
git clone https://github.com/StamusNetworks/SELKS
```



### 构建 Debain live （日文项目，很好的项目）

```
git clone https://github.com/homelith/debian-live-custom
```

网站详情：https://qiita.com/homelith/items/f30a1fbac89dc977c1ff



### 构建 Debian live （非常不错的项目）

```
git clone https://github.com/mafredri/debian-my-live-build
```



### 构建 Minimal linux live （研究linux系统基础）

```
git clone https://github.com/ivandavidov/minimal-linux-script
```



