---
title: Kali Linux 系统下解决 root 权限无法运行 Google Chrome
comments: true
tags: kali
categories: linux
keywords: 'kali,chrome'
description: root 运行 Google Chrome
abbrlink: b691c55
---



1. 修改`/usr/bin/google-chrome-stable`文件：
```
exec -a "$0" "$HERE/chrome" "$@" --no-sandbox --user-data-dir
```

2. Chromium 也而是类似，修改`/usr/share/applications/chromium.desktop`文件：
```
Exec=/usr/bin/chromium %U --no-sandbox --user-data-dir
```

3. 在系统语言为英文的情况下，设置启动 chrome 时显示为中文
修改`/usr/bin/google-chrome-stable`文件，添加一行即可:
```
export LANGUAGE=zh-cn /opt/google/chrome/google-chrome %U
```
