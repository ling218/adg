---
title: 制作自己的 Debian livecd
comments: true
tags: live-build
categories: live-build
keywords: 'live-build,linux,debian'
description: >-
  首先，Debian 官方是提供了 LiveCD 的，但是存在一些问题，其中一个最大的问题，就是不支持UEFI启动，另外官方提供的 LiveCD
  总是没有自己定制过的顺手，所以在这儿讲解一下如何创建一个自定义的 Debian LiveCD。
abbrlink: fa204dda
---



## 安装需要的软件包

`注意：` `#`号表示以 `root` 身份执行，下同。

```
# apt-get install \
	isolinux \
	syslinux \
	xorriso \
	debootstrap \
	squashfs-tools 
```



## 创建工作目录

```nohighlight
# mkdir ～/livecd
```

\##下载Debian安装镜像，取得必要的引导及配置文件
Debian安装镜像可以从 [这儿](http://mirrors.ustc.edu.cn/debian-cd/current/amd64/iso-cd/) 下载，得到文件 `debian-8.7.1-amd64-CD-1.iso` 。然后执行命令，将该文件挂载到系统中。

```nohighlight
# mount debian-8.7.1-amd64-CD-1.iso /mnt
```

然后拷贝其中的 `boot` 目录和 `isolinux` 目录到工作目录 `~/livecd` 中：

```nohighlight
# cp -rv /mnt/boot ~/livecd
# cp -rv /mnt/isolinux ~/livecd
```

然后修要分别修改配置文件 `~/livecd/boot/grub/grub.cfg` 和 `isolinux/isolinux.cfg` 内容如下：

### grub.cfg

```
if loadfont $prefix/font.pf2 ; then
  set gfxmode=800x600
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod png
  terminal_output gfxterm
fi

if background_image /isolinux/splash.png; then
  set color_normal=light-gray/black
  set color_highlight=white/black
else
  set menu_color_normal=cyan/blue
  set menu_color_highlight=white/blue
fi

insmod play
play 480 440 1
set timeout=0
menuentry --hotkey=i 'Install' {
    set background_color=black
    linux    /vmlinuz vga=788 boot=live quiet
    initrd   /initrd
}
```

### isolinux.cfg

```
# D-I config version 2.0
# search path for the c32 support libraries (libcom32, libutil etc.)
path 
menu hshift 7
menu width 61

menu title Debian GNU/Linux installer boot menu
menu background splash.png
menu color title	* #FFFFFFFF *
menu color border	* #00000000 #00000000 none
menu color sel		* #ffffffff #76a1d0ff *
menu color hotsel	1;7;37;40 #ffffffff #76a1d0ff *
menu color tabmsg	* #ffffffff #00000000 *
menu color help		37;40 #ffdddd00 #00000000 none
# XXX When adjusting vshift, take care that rows is set to a small
# enough value so any possible menu will fit on the screen,
# rather than falling off the bottom.
menu vshift 12
menu rows 10
menu helpmsgrow 15
# The command line must be at least one line from the bottom.
menu cmdlinerow 16
menu timeoutrow 16
menu tabmsgrow 18
menu tabmsg Press ENTER to boot or TAB to edit a menu entry
default install
label install
	menu label ^Install
	menu default
	kernel /vmlinuz
	append vga=788 initrd=/initrd boot=live quiet

#default vesamenu.c32
default install
prompt 0
timeout 0
```



## 用debootstrap构建一个最小环境

```nohighlight
# cd ~/livecd && su -
# debootstrap --include=linux-image-amd64,live-boot --component=main,contrib,non-free --arch=amd64 rootfs sid http://mirrors.163.com/debian/
```

然后 `chroot` 进入这个最小环境中，做一些定制工作，包括修改root密码，hostname，安装软件，创建用户什么的，这一步我就不展开讲了，根据你自己的需求定制即可。

```nohighlight
# chroot rootfs /bin/bash
```



## 将linux内核和initrd复制到工作目录的最顶层

```nohighlight
# cd ~/livecd
# cp rootfs/boot/linuz-*-amd64 vmlinuz
# cp rootfs/boot/initrd.img* initrd
```



## 构建squashfs压缩文件

进入工作目录 `~/livecd` 创建新目录 `live` 然后把刚才得到的 `rootfs` 目录压缩成squashfs格式的文件放在 `live`目录下，重命名为`filesystem.squashfs`。

```nohighlight
# mkdir live
# mksquashfs rootfs live/filesystem.squashfs
```

然后删除 `rootfs` 目录即可，或者把他移到其他地方，以备多次对 `LiveCD` 进行修改。

```nohighlight
# rm -rf rootfs
```



## 构建iso文件

最后一步就是构建定制的 `LiveCD`，这一步我们使用 `xorriso` 这个工具来进行。

```nohighlight
# cd livecd
# xorriso -as mkisofs -r -V 'LiveCD' \
    -J -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
    -J -joliet-long  \
    -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot \
    -boot-load-size 4 -boot-info-table -eltorito-alt-boot \
    -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus livecd/  \
    -o livecd.iso
```

完成之后，我们会得到一个 `livecd.iso` 的文件，这个就是我们制作出的 `LiveCD` ，可以把它dd到U盘中使用了。



## 文章地址

- https://forums.debiancn.org/t/debian-livecd/582（**Debian 中文论坛**）