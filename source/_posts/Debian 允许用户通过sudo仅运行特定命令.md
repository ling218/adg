---
title: Debian 允许用户通过sudo仅运行特定命令
comments: true
tags: sudo
categories: linux
keywords: 'linux,sudo'
description: Debian 允许用户通过sudo仅运行特定命令
abbrlink: 18f1a1f0
---


执行下面命令实现：

```
username ALL=(ALL) NOPASSWD:/bin/mkdir,/bin/rmdir
```

